void drawMap() {
  background(#C8C8C8);
  currentMap.draw();
  
  // display geolocation on mouse tip
  //de.fhpotsdam.unfolding.geo.Location location = map.getLocation(mouseX, mouseY);
  //fill(0);
  //text(location.getLat() + ", " + location.getLon(), mouseX, mouseY);
  
  //de.fhpotsdam.unfolding.geo.Location center = new de.fhpotsdam.unfolding.geo.Location(0.0f, 0.0f);
  //map.zoomAndPanTo(center, 2);
  //float maxPanningDistance = 100; // in km
  //map.setPanningRestriction(center, maxPanningDistance);
  
  //map.zoomAndPanTo(new Location(52.5f, 13.4f), 10); // zoom in
  
  /** draw markers **/
  for (int i = 0; i < twArray.size(); i++) {
    Tweet t = (Tweet) twArray.get(i);
    de.fhpotsdam.unfolding.geo.Location l = new de.fhpotsdam.unfolding.geo.Location(t.lat, t.lon);
    //de.fhpotsdam.unfolding.marker.SimplePointMarker marker = new de.fhpotsdam.unfolding.marker.SimplePointMarker(l);
    mk = new de.fhpotsdam.unfolding.marker.SimplePointMarker(l);
    
    ScreenPosition mkPos = mk.getScreenPosition(map);
    
    // draw markers
    
    //fill(#FFFFFF, 100); // plain
    if (t.sentiment > 0) {
      //fill(0, t.sentiment*10, 100, 100);
      fill(#FF0000, 100);
    }else if(t.sentiment == 0){
      fill(#000000, 100);
    }
    else {
      //fill(228, -10*t.sentiment, 100, 100);
      fill(#0000FF, 100);
    }
    
    // rain drop
    ellipse(mkPos.x, mkPos.y, t.r/2, t.r/2);
    //ellipse(mkPos.x, mkPos.y, t.r, t.r);
    fill(#000000, 255.0);
    ellipse(mkPos.x, mkPos.y, 2, 2);
    
    //////
//    pushStyle();
//    strokeWeight(1);
//    stroke(#FFFFFF,150);
//    noFill();
//    ellipse(mkPos.x, mkPos.y, t.r, t.r);
//    popStyle();

//    
//    map.addMarkers(mk);
//    
//    // marker style
//    marker.setColor(color(#FF0000));
//    marker.setStrokeColor(color(255, 0, 0));
//    marker.setStrokeWeight(1);
      
      
//    if (t.alphaVal < 3.0) {
//      twArray.remove(i);
//      i--;
//    }
  }

}
