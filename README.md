# TweetStreamProbe #

##Introduction

As the importance of social media in our daily life increases, most of people using it witness its significant impact on numerous practices such as business marketing, information science and social sciences. For instance, user-generated information from a few major microblogs are used in order to find consumer patterns on certain type of products. Furthermore, social scientists analyze and predict voting tendencies towards the candidates in a national election. There have been countless research projects conducted on social media datasets in the fields of information science, journalism and so on. In this project, we would like to experiment different visualization techniques with real-time data stream from the major microblog service: Twitter. The project is named as 'TweetProbe' since this visualization framework is designed to present patterns of metadata, topical distribution (in terms of emerging hashtags) and live activities on the current time-window. Particularly, the short time-window used in this project is the key component since it enables users of this application to detect real-time trends, local events, natural disasters and spikes of social signals at microscopic level in time frame.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact