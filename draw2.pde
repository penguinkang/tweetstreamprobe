void drawRT() {
  // every 1 sec
  if ( millis()-t1 > 1000 ) {
    numTw = bucTw;
    bucTw = 0;
    // reset t0
    t1 = millis();
  }

  // spacing
  float tall = h - hBorder*4;
  float wStep = w/(20 * rtNumShow);
  int rGap = 60;
  int numLogPanel = 6;
  float wBorder = w*0.8;
  int hHead = 3*hBorder;
  int hTail = h - hBorder;
  float hWidth = hTail - hHead;
  float barThickness = w*0.08;


  // draw timeline
  pushStyle();
  fill(#BBBBBB);
  textAlign(LEFT, TOP);
  textSize(15);
  text("Posted Since..", w - hBorder - rGap - 0.05*w, 2*hBorder);
  popStyle();

  pushAll();
  textSize(12);
  for (int i = 0; i < numLogPanel; i++ ) {
    for (int j = 1; j < 10; j++ ) {
      float pX = w - hBorder - rGap; // 20 px for label
      float pY = 3*hBorder + i*tall/numLogPanel + log10(j)*(tall/numLogPanel);

      if (j == 1) {
        stroke(#FFFFFF);
        //text(pow( 10, i)+" Min", w-hBorder-rGap, pY);
        text(timeLabels[i], w-hBorder-rGap, pY);
      }
      else { 
        stroke(#AAAAAA, 95);
      }

      line(pX, pY, pX - 0.05*w, pY);
    }
  }
  popAll();
  
  



  pushStyle();
//  int yPos = h - 250;
//  textAlign(LEFT, CENTER);
//  textSize(14);
//  //text("TOP " + rtNum + " RETWEETS", 10, yPos - 20);
//  text("TOP "+rtNumShow+" RETWEETS", 10, yPos - 20);
  
  ArrayList tempArray = (ArrayList<RT>)rtArray2.clone();
  
  // vars
  int numShow = min(tempArray.size(), rtNumShow);
  float total = 0;
  float[] percentile = new float[numShow];
  
  //for ( int i = 0; i < rtArray.size(); i++ ) {
  for ( int i = 0; i < numShow; i++ ) {
    RT rtweet;
    int numrt;
    int numrtMax;
    boolean nullException = false;
    try {
      rtweet = (RT)tempArray.get(i);
      numrt = (int)rtweet.getRtCount(); //// NULL POINTER EXCEPTION
      numrtMax = constrain(numrt/100, 1, int(0.8*w));
    } catch (Exception e){
      print(e);
      print("    ---> tempArray size: ");
      println(tempArray.size());
      
      // remove null exceptioned element from the array
      nullException = true;
      tempArray.remove(i);
      i--;
      continue;
    }
    float xStep = (w-(2 * hBorder))/2.0 * i/24.0;
    //int rx = int(hBorder + min(rtweet.ageLog, 24) * xStep);
    float rx = 0.2*w + i*0.05*w;
    int ry = int(3*hBorder + rtweet.ageLog10 * tall/numLogPanel);

    // timeline plot (DOTS)
    noStroke();
    fill(rankRT[i]);
    rectMode(CORNER);
    rect( w - hBorder - rGap - (rtNumShow-i)*wStep, ry, wStep, wStep ); // dots on the time line
    
    // compute real-time ratio
    percentile[i] = (float)rtweet.rtCount;
    total += rtweet.rtCount;
    
    // update Age
    rtweet.updateAge();

  }
  
  totalItems = (int)total;
  
  // bar animation
  buf2[10] = hTail;
  int prevX = hHead;
  int nextX = hHead;
  
  textAlign(LEFT, TOP);
  textFont(font, 18); // 16
  
  
  for ( int i = 0; i < numShow; i++ ) {
    RT rtweet;
    try {
      rtweet = (RT)tempArray.get(i);
    } catch (Exception e){
      print(e);
      print("\t");
      println(tempArray.size());
      continue;
    }
    
    percentile[i] = hWidth * (percentile[i] / total); // original
    // percentile[i] = hWidth * (log10(percentile[i])+1 / log10(total)+numShow); // log-scale
    nextX += (int) percentile[i];

    fill(rankRT[i]);
    
    // split animation (Converging algorithm)
    float xStart = hHead, xEnd = hTail;
    if ( i > 0 ) xStart = buf1[i] + int((prevX - buf1[i])*0.05);
    if ( i < (numShow-1) ){
      xEnd = buf2[i] + int((nextX - buf2[i])*0.05);
      buf2[i] = xEnd;
    }
    
    // boxes
    buf1[i] = xStart;
    
    noStroke();
    rect(hBorder, xStart, barThickness, xEnd - xStart);
    stroke(#AAAAAA);
    line(hBorder, xStart, hBorder + barThickness, xStart);
    text((String)rtweet.getText(), hBorder + barThickness + 10, xStart, w*0.75, (xEnd-xStart));
    fill(palette[1]);
    //text((int)rtweet.getRtCount(), hBorder + barThickness + 10, xStart);
    
    // labels
    fill(#000000);
    float lwidth = textWidth( Integer.toString(rtweet.newTw) ); // label width
    boolean showIt = 16 < (xEnd - xStart);  // if the label fits in the split
    if (showIt) text(Long.toString(rtweet.rtCount), hBorder+5, xStart);
    
    
    prevX = nextX;
  }
  popStyle();
}

