class RT 
{
  String msg;
  long id;
  int follower;
  int friend;
  long rtCount;
  String strTime;
  long ageMin;
  float ageLog10;
  int newTw;

  //specific to this particular visualization.
  float alphaVal;
  int rr; // ring radius

  RT(String _msg, long _id, int _follower, int _friend, long _rtCount, long _mins, String _strTime) {
    this.msg = _msg;
    this.id = _id;
    this.follower = _follower;
    this.friend = _friend;
    this.rtCount = _rtCount;
    
    this.strTime = _strTime;
    this.ageMin = _mins;
    this.ageLog10 = log10(max(abs(this.ageMin), 1)); // log age in minutes
    this.alphaVal = 100.0;
    this.rr = 1;
    this.newTw = 1;
  }

  public long getNewCount() {
    return this.newTw;
  }

  public long getRtCount() {
    return this.rtCount;
  }

  public String getText() {
    return this.msg;
  }

  public void updateRtCount(long _rtCount) {
    this.newTw += int(_rtCount - this.rtCount);
    this.rtCount = _rtCount;
  }
  
  public boolean equals (long _tid) {
    if (this.id == _tid) {
      return true;
    }
    return false;
  }
  
  public void updateAge(){
    this.ageMin = getAge(this.strTime);
    ageLog10 = log10(max(abs(this.ageMin), 1)); // log age in minutes
  }
  
  // update ring radius/alphaVal and return alphaVal
  public float getAlpha () {
    if (this.alphaVal < 0){
      this.alphaVal = 100.0;
      this.rr = 1;
    }else{
      alphaVal -= 10.0/( log(follower + 10)*10 ); // becomes slower as #follower increase in log scale. due to messenger effect.
      rr++; // increase ring radius by 1
    }
    return alphaVal;
  }
  
  public String toString () {
    return String.format("RT\nmsg: %s\nid: %s\nfo: %s\nfe: %s\nrtcount: %s\nmins: %s\nstrtime: %s\n", this.msg, this.id, this.follower, this.friend, this.rtCount, this.ageMin, this.strTime);
  }
}

