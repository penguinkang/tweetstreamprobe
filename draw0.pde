void viz_raindrop(){
  if ( millis()-t1 > 1000 ){
    now = getCurrentTime() + "\n" + title;
    numTw = bucTw;
    bucTw = 0;
    // reset t1
    t1 = millis();
  }

  if ( millis()-t3 > 10000 ) { // every 10 secs
    trimTweets(); // remove old tweets
    // reset t3
    t3 = millis();
  }

  // TIME LABEL
  pushAll();
  textFont(font1, 80);
  textAlign(LEFT, CENTER);
  //fill(#CCCCCC, 50);
  fill(#444444);
  float twidth = textWidth(now);
  text(now, w/2 - twidth/2, h/2);
  popAll();
  
  
  for (int i = 0; i < twArray.size(); i++) {
    Tweet t = (Tweet) twArray.get(i);
    if (t.alphaVal < 3.0) {
      twArray.remove(i);
      i--;
    }
  }

  for (int i = 0; i < twArray.size(); i++) {
    Tweet t = (Tweet) twArray.get(i);
    int colorIdx = 0;
    if (blink){
      colorIdx = int(random(0, 3.99));
    }
        
    t.rr += 1;
    t.alphaVal -= 20.0/( log(t.follower + 10)*3 );
    
    pushStyle();
    noFill();
    
    // ring animation
    if(blink){
      if (t.rt) stroke(#00FF00, t.alphaVal);
      else stroke(#FFFFFF, t.alphaVal);
      strokeWeight(3);
      ellipse(t.x, t.y, t.rr, t.rr);
    }
    
    noStroke();
    
    // green stroke for RT - DEPRECATED
//    if (t.rt) {
//      //fill(palette[4], t.alphaVal);
//      strokeWeight(2);
//      stroke(palette[4], t.alphaVal);
//    }
    
    if (t.sentiment > 0) {
      fill(0, t.sentiment*10, 100, t.alphaVal);
    }else if(t.sentiment == 0){
      fill(#FFFFFF, t.alphaVal);
    }
    else {
      fill(228, -10*t.sentiment, 100, t.alphaVal);
    }
    
    // rain drop
    ellipse(t.x, t.y, t.r, t.r);
    textFont(font, 17);
    text(t.location, t.x, t.y, 120, 100);
    //text(t.follower, t.x, t.y);
    //text(t.msg, t.x, t.y, 120, 100);
    //text(t.sentiment, t.x, t.y, 120, 100);
    
    //t.alphaVal--;
    popStyle();
  }
  

  
//  // render gauge
//  pushStyle();
//  fill(#FFFFFF, 70);
//  arc(width - 100, height*0.9, 50, 50, PI, 2*PI);
//  fill(#FF0000, 95);
//  arc(width - 100, height*0.9, 48, 48, PI, PI*(1 + float(numTw)/10.0));
//  
//  textAlign(CENTER, TOP);
//  fill(#999999, 90);
//  text(numTw + "tweets/s", width-100, height*0.9 + 10);
//  popStyle();
}
