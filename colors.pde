color[] palette0 = {#CE5F28, #CE9728, #CECE28, #FF0000};
color[] palette1 = {#CE5F28, #CE9728, #CECE28, #FF0000};
color[] palette2 = {#302D40, #475956, #90A67B, #FF0000}; // space
color[] palette3 = {#021859, #0433BF, #0460D9, #05F2F5, #010A26}; // moonlight
color[] palette4 = {#375C8C, #B8B0A1, #96E1FE, #FDFEE0, #8C6B42};
color[] palette5 = {#FF0000, #FF7400, #CD0074, #A60000};
color[] palette6 = {#03899C, #FF9C00, #FF4900, #015965};
color[] palette7 = {#3b5998, #8b9dc3, #f7f7f7, #dfe3ee}; // facebook
color[] palette8 = {#cceaf3, #c0deed, #33CCFF, #0084b4}; // twitter
color[] palette9 = {#33CCFF, #3B5998, #4875B4, #0084b4}; // linkedin
color[] sentiment = {#3FB8AF, #7FC7AF, #DAD8A7, #FF9E9D, #FF3D7F}; //-2 ~ +2

color[] rankRed = {#FFF7EC, #FEE8C8, #FDD49E, #FDBB84, #FC8D59, #EF6548, #D7301F, #B30000, #7F0000, #671414}; // 1 ~ 10
color[] rankBlue1 = {#FFFFFF, #F7FCF0, #E0F3DB, #CCEBC5, #A8DDB5, #7BCCC4, #4EB3D3, #2B8CBE, #0868AC, #084081};
color[] rankPink1 = {#FFFFFF, #FFF7F3, #FDE0DD, #FCC5C0, #FA9FB5, #F768A1, #DD3497, #AE017E, #7A0177, #49006A};
color[] dColor = new color[]{
    color(112, 11, 80), 
    color(244, 56, 80), 
    color(185, 58, 80), 
    color(139, 55, 80), 
    color(80, 65, 80), 
    color(61, 85, 80), 
    color(22, 81, 100), 
    color(8, 78, 100), 
    color(260, 47, 100), 
    color(306, 61, 80)
  };
color[] rank10 = rankRed;
color[] rankRT = rankBlue1;
color[] rankHT = rankPink1;
//color[] palette = {#CE5F28, #CE9728, #CECE28, #FF0000, #BCFFAD}; //dublin airport5

//color[] palette0 = {#F2D03B, #47D9BF, #00A1D9, #04518C, #003056}; // bahamas
//color[] palette1 = {#A3C46D, #8E9962, #703B35, #472528, #242126}; // venture capitalist
//color[] palette2 = {#E9F29D, #B7C29D, #878E8F, #67617A, #51456B};
//color[] palette3 = {#027373, #038C7F, #D9B343, #F28C3A, #BF3F34};
//color[] palette4 = {#E82C0C, #F0840C, #E5B600, #4F00FF, #DE0001};
//color[] palette5 = {#072111, #20421B, #AD8F15, #FF0011, #EB7500};
//color[] palette6 = {#FFB700, #E58D00, #BF7560, #7F5370, #402768};
//color[] palette7 = {#F33645, #F28907, #F2CB05, #1AC6D9, #9265A6};
//color[] palette8 = {#39060A, #85000C, #928E0F, #083952, #0A8ED2};
//color[] palette9 = {#F26B00, #D92800, #00010D, #8C030E, #590212};

String[] timeLabels = {"Now", "10 Min", "1.6 Hours", "16.7 Hours", "7 Days", "70 Days", "700 Days", "19 Years", "190 Years"};

