void draw() {
//  background(0,0,100);
  background(0,0,0);
  
  // time window reset
  int t = millis();
  if (t - t0 > 1000*600){ // time window = 10 min
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    t0 = millis();
  }
//  t1 = ( millis() - t0 ) / velo;
//  tDelta += t1;
//  t0 = millis();
//  if (t1 > 0) {
//    cam.setDistance( tDelta % maxDist, 0);
//  }

  // Auto switch between vis modes
  //:::: SWITCHING INTERVAL IN SEC :::://
  if (autoShift){
    int tnow = millis();
    vizMode = ((tnow - t2) / (1000 * s_interval)) % 4; // (switching between 0 ~ 4 in each min)
  }
  
  //// MODE 0: REAL-TIME RAINDROP ////
  if (vizMode == 0) {

    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe 2", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);
    text("#: " + title, 10, 40);
    
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME SENTIMENT MAP", w*0.88, 40);
    
    pushAll();
    textFont(font, 14);
    textAlign(LEFT, CENTER);
    rectMode(CENTER);
    for (int i = 9; i > -10; i--){
      if (i<0) fill(0, -10*i, 100);
      else  fill(228, 10*i, 100);
      rect( 10, 100+ 10*(i+9), 10, 10);
      
      if(i==-9) text(":)", 20, 100);
      if(i==9) text(":(", 20, 100 + 10*(i+9));
    }
    popAll();
    
    pushAll();
    fill(#999999);
    strokeWeight(3);
    stroke(#FFFFFF,95);
    line(100, 100, 100, h-100);
    line(w-100, 100, w-100, h-100);
    line(100, h-100, w-100, h-100);
    line(100, 100, w-100, 100);
    strokeWeight(1);
    
    int border = 100;
    textAlign(RIGHT, CENTER);
    for (int i=0; i< h-200 ; i+=100){
      line(border, h - border - i, w-border, h - border - i);
      if(i==0) text("0", border-5, h - border-i);
      else if(i%200==0) text("10x"+i/200, border-5, h - border - i);
    }
    
    textAlign(LEFT, TOP);
    for (int i=0; i< w-200 ; i+=100){
      line(border + i, h-border, border + i, border);
      if(i==0) text("0", border+i, h-border+5);
      else if(i%200==0) text("10x"+i/200, border+i, h-border+5);
    }
    
    popAll();
    
    pushAll();
    fill(#999999);
    textAlign(CENTER, CENTER);
    textFont(font, 14);
    text("FOLLOWERS", w/2, h-60);
    translate(40,h/2);
    rotate(-PI/2);
    text("RETWEET COUNT", 0, 0);
    popAll();
    
    viz_raindrop();
  }

  //// MODE 1: NEW RT ////
  else if (vizMode == 1) {
    
    // elapsed time
    if (timewatch){
      elapsedTimeVis();
    }

    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);
    text("#: " + title, 10, 40);
    
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME EMERGING RETWEETS", w*0.88, 40);
    
    fill(#FFFFFF);
    textSize(14);
    textAlign(RIGHT, BOTTOM);
    textLeading(10);
    text("Since "+(int)elapsedMin()+"\nmins ago", 70, 85);
    textAlign(LEFT, BOTTOM);
    fill(palette[3]);
    text("Total\nRetweets", 75, 85);
    drawRTUpdate();
  }

  //// MODE 2: RT ////
  else if (vizMode == 2) {
    
//---3D
    // elapsed time
    if (timewatch){
      elapsedTimeVis();
    }
    
    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);

    text("#: " + title, 10, 40);
    
    // subtitle
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME TOP 10 RETWEETS", w*0.88, 40);
    
    fill(#999999);
    textSize(14);
    textAlign(LEFT);
    text("Total Retweets", hBorder, 80);
    drawRT();
  }
  
  //// MODE 3:  ////
  else if (vizMode == 3) {

    // elapsed time
    if (timewatch){
      elapsedTimeVis();
    }

    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);

    text("#: " + title, 10, 40);
    
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME TOP 10 HASHTAGS", w*0.88, 40);
    
    fill(#999999);
    textSize(14);
    textAlign(LEFT);
    text("Total Hashtags", hBorder, 80);
    drawHT();
  }
  
  /** drawMap **/
  else if (vizMode == 4) {

    // elapsed time
//    if (timewatch){
//      elapsedTimeVis();
//    }
    
    drawMap();
    
    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(#000000);//fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(#000000);//fill(palette[1]);

    text("#: " + title, 10, 40);
    
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME DISTRIBUTION", w*0.88, 40);
    
//    fill(#999999);
//    textSize(14);
//    textAlign(LEFT);
//    text("Total Messages", hBorder, 80);
    
  }

}

