import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.text.DateFormat; 
import java.text.SimpleDateFormat; 
import java.util.Date; 
import java.util.Locale; 
import java.util.Comparator; 
import java.util.Collections; 
import java.util.StringTokenizer; 
import java.util.Map; 
import processing.opengl.*; 
import javax.media.opengl.*; 
import peasy.*; 
import controlP5.*; 
import de.bezier.data.sql.*; 
import de.fhpotsdam.unfolding.*; 
import de.fhpotsdam.unfolding.geo.*; 
import de.fhpotsdam.unfolding.utils.*; 
import de.fhpotsdam.unfolding.providers.*; 

import twitter4j.examples.block.*; 
import twitter4j.examples.trends.*; 
import twitter4j.conf.*; 
import twitter4j.json.*; 
import twitter4j.internal.async.*; 
import twitter4j.internal.logging.*; 
import twitter4j.api.*; 
import twitter4j.internal.json.*; 
import twitter4j.examples.friendsandfollowers.*; 
import twitter4j.*; 
import twitter4j.examples.directmessage.*; 
import twitter4j.media.*; 
import twitter4j.examples.list.*; 
import twitter4j.examples.stream.*; 
import twitter4j.examples.search.*; 
import twitter4j.examples.friendship.*; 
import twitter4j.examples.timeline.*; 
import twitter4j.util.*; 
import twitter4j.examples.tweets.*; 
import twitter4j.examples.user.*; 
import twitter4j.examples.async.*; 
import twitter4j.examples.help.*; 
import twitter4j.examples.media.*; 
import twitter4j.auth.*; 
import twitter4j.internal.util.*; 
import twitter4j.examples.account.*; 
import twitter4j.examples.geo.*; 
import twitter4j.internal.http.*; 
import twitter4j.examples.spamreporting.*; 
import twitter4j.examples.oauth.*; 
import twitter4j.examples.favorite.*; 
import twitter4j.examples.json.*; 
import twitter4j.examples.notification.*; 
import twitter4j.examples.listsubscribers.*; 
import twitter4j.management.*; 
import twitter4j.examples.listmembers.*; 
import twitter4j.examples.savedsearches.*; 
import twitter4j.examples.legal.*; 
import twitter4j.internal.org.json.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class TweetStreamProbe extends PApplet {

 /*
 TWEETPROBE
 Twitter Stream Vizualization
 BYUNGKYU (JAY) KANG
 ver1.0 - June. 01. 2013.
 Presented and exhibited in IEEE VIS 2013 Arts Program.
 
 ver 1.1 - May. 21. 2015.
 Presented in MAT End-of-Year-Show (EOYS), University of California, Santa Barbara.
 */

//import java.text.*;
//import java.util.*;











    // interface
 // mysql
 // interactive map




// create objects
ControlP5 controlp5;  // controlP5 object
Prop props;           // property object
MySQL db;             // mysql object
UnfoldingMap map;     // map interface object initialized
de.fhpotsdam.unfolding.marker.SimplePointMarker mk; // marker obj
 
// DEFAULT KEYWORDS TO SEARCH
String keywords[] = { "oil" };

// OAuth key containers
static String cKey;
static String cSec;
static String aToken;
static String aTokenSec;

String lang[] = {"en"};
int[] palette = { 0xffFFFFFF, 0xffAFEFF1, 0xff31C4F5, 0xff0084B4, 0xff97CD39 }; //twitter base
int[] paletteT = { 0xff302D40, 0xff475956, 0xff90A67B, 0xffFF0000 }; // twitter
int[] paletteRT = { 0xffFF0000, 0xffFF7400, 0xffCD0074, 0xffA60000 }; // RT

int w, h; // screen size
int hBorder = 30;
PImage   img;

int      vizMode = 4; // visualization type at start
boolean  imageLoaded;
boolean  displayImg = false;
boolean  blink = true;
boolean  timewatch = true;
boolean  autoShift = false;
boolean  ui = true;

PFont font, font1, fontb, font0;
String title;
String who, time, txt, where, now, startTime;

ArrayList twArray = new ArrayList(); // Tweet Bucket
ArrayList rtArray1 = new ArrayList(); // Emerging RT Ranking view
ArrayList rtArray2 = new ArrayList(); // RT Count Ranking view
ArrayList htArray = new ArrayList(); // Hashtag Bucket

int t0, t1, t2, t3, tDelta; // timestamp
int s_interval = 30; // auto shift default interval
int velo = 10;
int maxDist = 1000;
int numTw, bucTw; // num of tweet per sec / bucket
int htNum = 30; // top N hashtags
int rtNum = 30;  // top N retweets
int rtNumShow = 10;
int htNumShow = 10;
int totalItems = 0;
int burstThreshold = 10; // BURST THRESHOLD
int htBurstThreshold = 20; // BURST THRESHOLD
float[] buf1     = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, w };
float[] buf2     = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, w };

// corpuses
String[] posCorpus, negCorpus;


public void setup() {
  
  // create i/f objects
  controlp5 = new ControlP5(this);
  
  //map = new UnfoldingMap(this, new StamenMapProvider.Toner());
  //map = new UnfoldingMap(this, new EsriProvider.WorldShadedRelief());
  //map = new UnfoldingMap(this, new StamenMapProvider.TonerBackground());
  //map = new UnfoldingMap(this, new OpenWeatherProvider.Rain());
  //map = new UnfoldingMap(this, new OpenWeatherProvider.Temperature());
  map = new UnfoldingMap(this, new OpenMapSurferProvider.Grayscale());
  //map = new UnfoldingMap(this, new Microsoft.AerialProvider());
  //map = new UnfoldingMap(this, new ImmoScout.ImmoScoutProvider());
  
  //map = new UnfoldingMap(this);
  MapUtils.createDefaultEventDispatcher(this, map);
  
  // property setup
  try {
    props = new Prop();
    // load a configuration from a file inside the data folder
    props.load("conf.properties");
    
    /** Please uncomment this if you want to read width/height from property file **/
    //w = int(props.getProperty("prop.width",""+1024));
    //h = int(props.getProperty("prop.height",""+768));

    w = displayWidth;
    h = displayHeight;
    //w = 1024;
    //h = 768;
    
    // Your Oauth info
    cKey     = props.getProperty("prop.consumerKey","");
    cSec     = props.getProperty("prop.consumerSecret","");
    // Your Access Token info
    aToken    = props.getProperty("prop.accessToken","");
    aTokenSec = props.getProperty("prop.accessTokenSecret","");
    
    // debug
    println(cKey+"\n"+cSec+"\n"+aToken+"\n"+aTokenSec);
    
  }catch (Exception e) {
    println("can't find config file...");
  }
  
  // database setup
  String user     = "root";
  String pass     = "kang4848";
  String database = "geocode";

  db = new MySQL( this, "localhost", database, user, pass );  // open database file
  db.setDebug(false);
  
 
  // make a title
  title = "";
  for (int i = 0; i < keywords.length; i++ ) {
    title += keywords[i] + " ";
  }  
  
  // canvas setup
  size(w, h, OPENGL);
  /** deprecated **/
  //hint(DISABLE_OPENGL_2X_SMOOTH);
  //hint(ENABLE_OPENGL_4X_SMOOTH);

  noStroke();
  imageMode(CORNER);
  colorMode(HSB, 360, 100, 100);
  img = loadImage("logo.png");
  smooth();
  
  // font setup
  font = loadFont("Helvetica-Bold-22.vlw");
  fontb = loadFont("Helvetica-Bold-36.vlw");
  font0 = loadFont("Helvetica-14.vlw");
  font1 = loadFont("HelveticaNeue-Bold-180.vlw");
  textFont(font0, 14);
  strokeCap(SQUARE);
  textAlign(LEFT, CENTER);
  
  // initialize Twitter Stream session
  connectTwitter();
  twitter.addListener(listener); // start another thread
  if (keywords.length==0) twitter.sample();
  else twitter.filter(new FilterQuery().track(keywords));
  
  // start counter
  t0 = millis();
  t1 = millis();
  t2 = millis();
  t3 = millis(); // to trim tweets in view0 (raindrop)
  now = getCurrentTime();
    
  posCorpus = loadStrings("positive.txt");
  negCorpus = loadStrings("negative.txt");
  
  frameRate(60);
  
  if (ui){
    controlp5.addTextfield("query")
       .setPosition(w - hBorder - 60 - 0.05f*w,20)
       .setSize(100,20)
       .setFont(font)
       .setFocus(true)
       .setColor(0xffffffff)
       ;
       
    controlp5.addToggle("show",autoShift,300,10,20,20);
    controlp5.addToggle("wave",blink,340,10,20,20);
    controlp5.addToggle("clock",timewatch,380,10,20,20);
    controlp5.addSlider("interval",5,600,10,420,10,100,20);
  }
}

// displaying tweets
public void arriveTw (Status s)
{
  if ( vizMode == 0 ) {
    raindrop( s );
  }
  else if( vizMode == 1 ) {
    readRTUpdate( s );
  }
  else if( vizMode == 2 ) {
    readRT( s );
  }
  else if( vizMode == 3 ) {
    readHT( s );
  }
  else if( vizMode == 4 ) {
    readLocation( s );
  }
}

class HT 
{
  String txt;
  String strTime;
  int htCount;
  long ageMin;
  float ageLog10;

  HT( String _txt, long _mins, String _time) {
    txt = _txt;
    this.htCount = 1;
    this.strTime = _time;
    this.ageMin = _mins;
    this.ageLog10 = log10(max(abs(this.ageMin), 1)); // log age in minutes
  }

  public int getHtCount() {
    return this.htCount;
  }

  public String getText() {
    return this.txt;
  }

  public void update(long _mins, String _time) {
    this.htCount++;
    if (_mins > this.ageMin){
      this.ageMin = _mins;
      this.ageLog10 = log10(max(abs(this.ageMin), 1));
      this.strTime = _time;
    }
  }

  public void updateAge(){
    this.ageMin = getAge(this.strTime);
    ageLog10 = log10(max(abs(this.ageMin), 1)); // log age in minutes
  }

  public boolean equals (String x) {
    if (this.txt.equals(x)) { 
      return true;
    }
    else {
      return false;
    }
  }
}

public void keyPressed() {
  // palette toggle
//  if ( key == 'b' ) {
//    blink = !blink;
//  }
//  if ( key == 'a' ) {
//    autoShift = !autoShift;
//  }
  if ( key == '0' ) {
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    t0 = millis();
    vizMode = 0;
  }
  if ( key == '1' ) {
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    t0 = millis();
    vizMode = 1;
  }
  if ( key == '2' ) {
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    t0 = millis();
    vizMode = 2;
  }
  if ( key == '3' ) {
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    t0 = millis();
    vizMode = 3;
    fill(0xffFFFFFF);
  }
  if ( key == '4' ) {
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    t0 = millis();
    vizMode = 4;
    fill(0xffFFFFFF);
  }
//  if ( key == 't' ) {
//    timewatch = !timewatch;
//  }
}

class Prop {
  String[] lines;
  
  // load property file (relative path)
  public void load (String path){
    this.lines = loadStrings(path); 
  }
  
  public String getProperty( String propName, String defVal ){
    for (int i=0; i<lines.length; i++){
      if (lines[i].length() > 2 && !lines[i].substring(0, 1).matches("#")){
        if (lines[i].contains(propName)) return lines[i].split("=")[1];
      }
    }
    return defVal;
  }
}
class RT 
{
  String msg;
  long id;
  int follower;
  int friend;
  long rtCount;
  String strTime;
  long ageMin;
  float ageLog10;
  int newTw;

  //specific to this particular visualization.
  float alphaVal;
  int rr; // ring radius

  RT(String _msg, long _id, int _follower, int _friend, long _rtCount, long _mins, String _strTime) {
    this.msg = _msg;
    this.id = _id;
    this.follower = _follower;
    this.friend = _friend;
    this.rtCount = _rtCount;
    
    this.strTime = _strTime;
    this.ageMin = _mins;
    this.ageLog10 = log10(max(abs(this.ageMin), 1)); // log age in minutes
    this.alphaVal = 100.0f;
    this.rr = 1;
    this.newTw = 1;
  }

  public long getNewCount() {
    return this.newTw;
  }

  public long getRtCount() {
    return this.rtCount;
  }

  public String getText() {
    return this.msg;
  }

  public void updateRtCount(long _rtCount) {
    this.newTw += PApplet.parseInt(_rtCount - this.rtCount);
    this.rtCount = _rtCount;
  }
  
  public boolean equals (long _tid) {
    if (this.id == _tid) {
      return true;
    }
    return false;
  }
  
  public void updateAge(){
    this.ageMin = getAge(this.strTime);
    ageLog10 = log10(max(abs(this.ageMin), 1)); // log age in minutes
  }
  
  // update ring radius/alphaVal and return alphaVal
  public float getAlpha () {
    if (this.alphaVal < 0){
      this.alphaVal = 100.0f;
      this.rr = 1;
    }else{
      alphaVal -= 10.0f/( log(follower + 10)*10 ); // becomes slower as #follower increase in log scale. due to messenger effect.
      rr++; // increase ring radius by 1
    }
    return alphaVal;
  }
  
  public String toString () {
    return String.format("RT\nmsg: %s\nid: %s\nfo: %s\nfe: %s\nrtcount: %s\nmins: %s\nstrtime: %s\n", this.msg, this.id, this.follower, this.friend, this.rtCount, this.ageMin, this.strTime);
  }
}

class Tweet 
{
  String msg;
  boolean rt;
  int follower;
  int friend;
  int sentiment;

  //specific to this particular visualization.
  float x, y;
  double lat, lon;
  float alphaVal;
  float time;
  String location;
  int r; // circle radius
  int rr; // ring radius

  Tweet(String _msg, boolean _retweet, int _rtCount, int _follower, int _friend, String _location, int _sentiment) {
    msg = _msg;
    
//    this.lat = null;
//    this.lon = null;
    
    // x, y coordinates
    this.x = (int)min(100 + log10(_follower)*200, w-100);
    this.y = (int)max(h - 100 - log10(_rtCount+1)*200, 0+100); // +1
    
    rt = _retweet;
    alphaVal = 255.0f;
    follower = _follower;
    r = constrain(PApplet.parseInt(log10(_follower + 10)*10), 3, 100);
    rr = 1;
    friend = _friend;
    location = _location;
    sentiment = _sentiment;
    this.time = millis();
  }
  
  public void setGeoCoordinate (double lat, double lon){
    this.lat = lat;
    this.lon = lon;
  }
  
  public boolean elapsed (){
    boolean del = millis() - this.time > 30*1000 ? true : false;
    return del;
  }
}

/* Twitter4J library is used */

///////////////////////////// End Variable Config ////////////////////////////

TwitterStream twitter = new TwitterStreamFactory().getInstance();

// Initial connection
public void connectTwitter() {
  twitter.setOAuthConsumer(cKey, cSec);
  AccessToken accessToken = loadAccessToken();
  twitter.setOAuthAccessToken(accessToken);
}

// Loading up the access token
private static AccessToken loadAccessToken() {
  return new AccessToken(aToken, aTokenSec);
}

// This listens for new tweet
StatusListener listener = new StatusListener() {
  public void onStatus(Status status) {

    //println("@" + status.getUser().getScreenName() + " - " + status.getText());
    arriveTw(status);
    
  }

  public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
    //System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
  }
  public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
    //  System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
  }
  public void onScrubGeo(long userId, long upToStatusId) {
    System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
  }

  public void onException(Exception ex) {
    ex.printStackTrace();
  }
};
int[] palette0 = {0xffCE5F28, 0xffCE9728, 0xffCECE28, 0xffFF0000};
int[] palette1 = {0xffCE5F28, 0xffCE9728, 0xffCECE28, 0xffFF0000};
int[] palette2 = {0xff302D40, 0xff475956, 0xff90A67B, 0xffFF0000}; // space
int[] palette3 = {0xff021859, 0xff0433BF, 0xff0460D9, 0xff05F2F5, 0xff010A26}; // moonlight
int[] palette4 = {0xff375C8C, 0xffB8B0A1, 0xff96E1FE, 0xffFDFEE0, 0xff8C6B42};
int[] palette5 = {0xffFF0000, 0xffFF7400, 0xffCD0074, 0xffA60000};
int[] palette6 = {0xff03899C, 0xffFF9C00, 0xffFF4900, 0xff015965};
int[] palette7 = {0xff3b5998, 0xff8b9dc3, 0xfff7f7f7, 0xffdfe3ee}; // facebook
int[] palette8 = {0xffcceaf3, 0xffc0deed, 0xff33CCFF, 0xff0084b4}; // twitter
int[] palette9 = {0xff33CCFF, 0xff3B5998, 0xff4875B4, 0xff0084b4}; // linkedin
int[] sentiment = {0xff3FB8AF, 0xff7FC7AF, 0xffDAD8A7, 0xffFF9E9D, 0xffFF3D7F}; //-2 ~ +2

int[] rankRed = {0xffFFF7EC, 0xffFEE8C8, 0xffFDD49E, 0xffFDBB84, 0xffFC8D59, 0xffEF6548, 0xffD7301F, 0xffB30000, 0xff7F0000, 0xff671414}; // 1 ~ 10
int[] rankBlue1 = {0xffFFFFFF, 0xffF7FCF0, 0xffE0F3DB, 0xffCCEBC5, 0xffA8DDB5, 0xff7BCCC4, 0xff4EB3D3, 0xff2B8CBE, 0xff0868AC, 0xff084081};
int[] rankPink1 = {0xffFFFFFF, 0xffFFF7F3, 0xffFDE0DD, 0xffFCC5C0, 0xffFA9FB5, 0xffF768A1, 0xffDD3497, 0xffAE017E, 0xff7A0177, 0xff49006A};
int[] dColor = new int[]{
    color(112, 11, 80), 
    color(244, 56, 80), 
    color(185, 58, 80), 
    color(139, 55, 80), 
    color(80, 65, 80), 
    color(61, 85, 80), 
    color(22, 81, 100), 
    color(8, 78, 100), 
    color(260, 47, 100), 
    color(306, 61, 80)
  };
int[] rank10 = rankRed;
int[] rankRT = rankBlue1;
int[] rankHT = rankPink1;
//color[] palette = {#CE5F28, #CE9728, #CECE28, #FF0000, #BCFFAD}; //dublin airport5

//color[] palette0 = {#F2D03B, #47D9BF, #00A1D9, #04518C, #003056}; // bahamas
//color[] palette1 = {#A3C46D, #8E9962, #703B35, #472528, #242126}; // venture capitalist
//color[] palette2 = {#E9F29D, #B7C29D, #878E8F, #67617A, #51456B};
//color[] palette3 = {#027373, #038C7F, #D9B343, #F28C3A, #BF3F34};
//color[] palette4 = {#E82C0C, #F0840C, #E5B600, #4F00FF, #DE0001};
//color[] palette5 = {#072111, #20421B, #AD8F15, #FF0011, #EB7500};
//color[] palette6 = {#FFB700, #E58D00, #BF7560, #7F5370, #402768};
//color[] palette7 = {#F33645, #F28907, #F2CB05, #1AC6D9, #9265A6};
//color[] palette8 = {#39060A, #85000C, #928E0F, #083952, #0A8ED2};
//color[] palette9 = {#F26B00, #D92800, #00010D, #8C030E, #590212};

String[] timeLabels = {"Now", "10 Min", "1.6 Hours", "16.7 Hours", "7 Days", "70 Days", "700 Days", "19 Years", "190 Years"};

//// [0] REALTIME RAIN DROP VIZ ////
public void raindrop( Status s ) {
  who = s.getUser().getScreenName();

  int follower = s.getUser().getFollowersCount();
  int friend = s.getUser().getFriendsCount();
  String location = s.getUser().getLocation();

  time = s.getCreatedAt().toString();
  txt = s.getText();
  
  // sentiment extraction
  int sentiment = getSentiment( txt );
  
  boolean retweet = false;
  String[] m1 = match(txt, "RT @");
  if (m1 != null) {
    retweet = true;
  }

  GeoLocation g =s.getGeoLocation();
  if (g!=null){
    where = g.toString();
  }
  
  int rtCount = 0;
  if (s.getRetweetedStatus() != null) rtCount = (int)s.getRetweetedStatus().getRetweetCount();
  //else println("no rt!");
  twArray.add(new Tweet(txt, retweet, rtCount, follower, friend, location, sentiment));
  bucTw++;
}

//// [1] RTUpdate Process ////
public void readRTUpdate( Status s ) {
  bucTw++;

  // if tw origin exists...
  if (s.getRetweetedStatus() != null) {
    String rtText = s.getRetweetedStatus().getText();
    rtText = rtText.replace("\n", "").replace("\r", "");
    int rtFollower = s.getRetweetedStatus().getUser().getFollowersCount();
    int rtFriend = s.getRetweetedStatus().getUser().getFriendsCount();
    long rtId = s.getRetweetedStatus().getId();
    long rtCount = s.getRetweetedStatus().getRetweetCount();
    String time = s.getRetweetedStatus().getCreatedAt().toString();
    
    // add to rtArray
    boolean exist = false;
    if (vizMode == 1) exist = !rtExist(rtArray1, rtId, rtCount);
    else  exist = !rtExist(rtArray2, rtId, rtCount);
    if (exist) {
      long mins = getAge(time);
      
      if (vizMode == 1) rtArray1.add(new RT(rtText, rtId, rtFollower, rtFriend, rtCount, mins, time));
      else rtArray2.add(new RT(rtText, rtId, rtFollower, rtFriend, rtCount, mins, time));
    }
    
    // sort RT when it arrives
    if (vizMode == 1) sortNewRT(rtArray1);
    else sortNewRT(rtArray2);
  }
}


//// [2] RT Process ////
public void readRT( Status s ) {
  bucTw++;

  // if tw origin exists...
  if (s.getRetweetedStatus() != null) {
    String rtText = s.getRetweetedStatus().getText();
    rtText = rtText.replace("\n", "").replace("\r", "");
    int rtFollower = s.getRetweetedStatus().getUser().getFollowersCount();
    int rtFriend = s.getRetweetedStatus().getUser().getFriendsCount();
    long rtId = s.getRetweetedStatus().getId();
    long rtCount = s.getRetweetedStatus().getRetweetCount();
    String time = s.getRetweetedStatus().getCreatedAt().toString();
    
    // add to rtArray
    boolean exist = false;
    if (vizMode == 1) exist = !rtExist(rtArray1, rtId, rtCount);
    else  exist = !rtExist(rtArray2, rtId, rtCount);
    if (exist) {
      long mins = getAge(time);
      
      if (vizMode == 1) rtArray1.add(new RT(rtText, rtId, rtFollower, rtFriend, rtCount, mins, time));
      else rtArray2.add(new RT(rtText, rtId, rtFollower, rtFriend, rtCount, mins, time));
    }
    
    // sort RT when it arrives
    if (vizMode == 1) sortRT(rtArray1);
    else sortRT(rtArray2);
  }
}

//// [3] HT Process ////
public void readHT( Status s ) {
  // gloval vars
//  who = s.getUser().getScreenName();

  // local vars
//  int follower = s.getUser().getFollowersCount();
//  int friend = s.getUser().getFriendsCount();
//  String profLinkColor = s.getUser().getProfileLinkColor();
//  time = s.getCreatedAt().toString();
  String time;
  if (s.getRetweetedStatus() != null) time = s.getRetweetedStatus().getCreatedAt().toString();
  else time = s.getCreatedAt().toString();
  long mins = getAge(time);
  txt = s.getText();

  // RT? - by text
  boolean retweet = false;
  String[] m1 = match(txt, "RT @");
  if (m1 != null) {
    retweet = true;
  }

  // GeoLocation
//  GeoLocation g =s.getGeoLocation();
//  if (g!=null)where = g.toString();

  bucTw++;

  // HASHTAG collection
  // String[] txts = txt.split("\\W");
  txt = txt.toUpperCase();
  txt = txt.replace("...", "");
  String[] txts = txt.split("\\s+");
  for (int i = 0; i < txts.length; i++) {
    // if HT
    if ( txts[i].startsWith("#") ) {
      txts[i] = txts[i].replace(",","").replace(".","").replace(":","");
      
      // if exists
      if (!htExist(txts[i], mins, time)) {
        
        // add to the list
        htArray.add(new HT(txts[i], mins, time));
        
        // sort HT when it arrives
        sortHashtag();
      }
    }
  }

}


//// [4] GeoCode Process ////
public void readLocation( Status s ) {
  //who = s.getUser().getScreenName();

  int follower = s.getUser().getFollowersCount();
  int friend = s.getUser().getFriendsCount();
  String location = s.getUser().getLocation();

  //time = s.getCreatedAt().toString();
  txt = s.getText();
  
  // sentiment extraction
  int sentiment = getSentiment( txt );
  
  // detect RT
  boolean retweet = false;
  String[] m1 = match(txt, "RT @");
  if (m1 != null) {
    retweet = true;
  }
  
  // RT count
  int rtCount = 0;
  if (s.getRetweetedStatus() != null) rtCount = (int)s.getRetweetedStatus().getRetweetCount();
  //else println("no rt!");
  
  Tweet tw = new Tweet(txt, retweet, rtCount, follower, friend, location, sentiment);
  
  // geo only
  GeoLocation g =s.getGeoLocation();
  if (g!=null){
    where = g.toString();
    //println(where);
    tw.setGeoCoordinate(g.getLatitude(), g.getLongitude());
    twArray.add(tw);
    bucTw++;
    
  }else{
    /** @TODO: add interpretation logic here! **/
    // sql open connection
    //for (String word: location.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+")){
    for (String word: location.toLowerCase().replace("\n",",").split(",")){
      word = word.trim();
      System.out.println(word);
      if ( db.connect() ){
        //String _query = "SELECT * FROM " + "all15k" + " WHERE `name` LIKE '%"+ word + "%' LIMIT 1";
        String _query = "SELECT * FROM " + "`all`" + " WHERE LOWER(`name`) LIKE '"+ word + "' LIMIT 1"; 
        db.query( _query );
        if ( db.next() ){ // if matches
          float lat = db.getFloat("lat");
          float lon = db.getFloat("lon");
          if (lat != 0){
            tw.setGeoCoordinate(lat, lon);
            twArray.add(tw);
            bucTw++;
            println("lat:"+lat+"  lon:"+lon);
            println("=====");
            break;
          }
        }
      }
      db.close();
    }
    // getGeocode
    // sql close connection
    //tw.setGeoCoordinate(0.0,0.0);
    
    System.gc(); // manually collect garbages
  }
}
public void draw() {
//  background(0,0,100);
  background(0,0,0);
  
  // time window reset
  int t = millis();
  if (t - t0 > 1000*600){ // time window = 10 min
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    t0 = millis();
  }
//  t1 = ( millis() - t0 ) / velo;
//  tDelta += t1;
//  t0 = millis();
//  if (t1 > 0) {
//    cam.setDistance( tDelta % maxDist, 0);
//  }

  // Auto switch between vis modes
  //:::: SWITCHING INTERVAL IN SEC :::://
  if (autoShift){
    int tnow = millis();
    vizMode = ((tnow - t2) / (1000 * s_interval)) % 4; // (switching between 0 ~ 4 in each min)
  }
  
  //// MODE 0: REAL-TIME RAINDROP ////
  if (vizMode == 0) {

    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe 2", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);
    text("#: " + title, 10, 40);
    
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME SENTIMENT MAP", w*0.88f, 40);
    
    pushAll();
    textFont(font, 14);
    textAlign(LEFT, CENTER);
    rectMode(CENTER);
    for (int i = 9; i > -10; i--){
      if (i<0) fill(0, -10*i, 100);
      else  fill(228, 10*i, 100);
      rect( 10, 100+ 10*(i+9), 10, 10);
      
      if(i==-9) text(":)", 20, 100);
      if(i==9) text(":(", 20, 100 + 10*(i+9));
    }
    popAll();
    
    pushAll();
    fill(0xff999999);
    strokeWeight(3);
    stroke(0xffFFFFFF,95);
    line(100, 100, 100, h-100);
    line(w-100, 100, w-100, h-100);
    line(100, h-100, w-100, h-100);
    line(100, 100, w-100, 100);
    strokeWeight(1);
    
    int border = 100;
    textAlign(RIGHT, CENTER);
    for (int i=0; i< h-200 ; i+=100){
      line(border, h - border - i, w-border, h - border - i);
      if(i==0) text("0", border-5, h - border-i);
      else if(i%200==0) text("10x"+i/200, border-5, h - border - i);
    }
    
    textAlign(LEFT, TOP);
    for (int i=0; i< w-200 ; i+=100){
      line(border + i, h-border, border + i, border);
      if(i==0) text("0", border+i, h-border+5);
      else if(i%200==0) text("10x"+i/200, border+i, h-border+5);
    }
    
    popAll();
    
    pushAll();
    fill(0xff999999);
    textAlign(CENTER, CENTER);
    textFont(font, 14);
    text("FOLLOWERS", w/2, h-60);
    translate(40,h/2);
    rotate(-PI/2);
    text("RETWEET COUNT", 0, 0);
    popAll();
    
    viz_raindrop();
  }

  //// MODE 1: NEW RT ////
  else if (vizMode == 1) {
    
    // elapsed time
    if (timewatch){
      elapsedTimeVis();
    }

    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);
    text("#: " + title, 10, 40);
    
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME EMERGING RETWEETS", w*0.88f, 40);
    
    fill(0xffFFFFFF);
    textSize(14);
    textAlign(RIGHT, BOTTOM);
    textLeading(10);
    text("Since "+(int)elapsedMin()+"\nmins ago", 70, 85);
    textAlign(LEFT, BOTTOM);
    fill(palette[3]);
    text("Total\nRetweets", 75, 85);
    drawRTUpdate();
  }

  //// MODE 2: RT ////
  else if (vizMode == 2) {
    
//---3D
    // elapsed time
    if (timewatch){
      elapsedTimeVis();
    }
    
    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);

    text("#: " + title, 10, 40);
    
    // subtitle
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME TOP 10 RETWEETS", w*0.88f, 40);
    
    fill(0xff999999);
    textSize(14);
    textAlign(LEFT);
    text("Total Retweets", hBorder, 80);
    drawRT();
  }
  
  //// MODE 3:  ////
  else if (vizMode == 3) {

    // elapsed time
    if (timewatch){
      elapsedTimeVis();
    }

    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);

    text("#: " + title, 10, 40);
    
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME TOP 10 HASHTAGS", w*0.88f, 40);
    
    fill(0xff999999);
    textSize(14);
    textAlign(LEFT);
    text("Total Hashtags", hBorder, 80);
    drawHT();
  }
  
  /** drawMap **/
  else if (vizMode == 4) {

    // elapsed time
    if (timewatch){
      elapsedTimeVis();
    }
    
    drawMap();
    
    // APPLICATION TITLE
    textAlign(LEFT, CENTER);
    image(img, 10, 10, 20, 20);
    fill(palette[4]);
    textFont(fontb, 20);
    text("Tweet Stream Probe", 40, 20);
  
    // title
    noStroke();
    textFont(font, 20);
    fill(palette[1]);

    text("#: " + title, 10, 40);
    
    fill(palette[3]);
    textAlign(RIGHT);
    text("REAL-TIME DISTRIBUTION", w*0.88f, 40);
    
    fill(0xff999999);
    textSize(14);
    textAlign(LEFT);
    text("Total Hashtags", hBorder, 80);
    
  }

}

public void viz_raindrop(){
  if ( millis()-t1 > 1000 ){
    now = getCurrentTime() + "\n" + title;
    numTw = bucTw;
    bucTw = 0;
    // reset t1
    t1 = millis();
  }

  if ( millis()-t3 > 10000 ) { // every 10 secs
    trimTweets(); // remove old tweets
    // reset t3
    t3 = millis();
  }

  // TIME LABEL
  pushAll();
  textFont(font1, 80);
  textAlign(LEFT, CENTER);
  //fill(#CCCCCC, 50);
  fill(0xff444444);
  float twidth = textWidth(now);
  text(now, w/2 - twidth/2, h/2);
  popAll();
  
  
  for (int i = 0; i < twArray.size(); i++) {
    Tweet t = (Tweet) twArray.get(i);
    if (t.alphaVal < 3.0f) {
      twArray.remove(i);
      i--;
    }
  }

  for (int i = 0; i < twArray.size(); i++) {
    Tweet t = (Tweet) twArray.get(i);
    int colorIdx = 0;
    if (blink){
      colorIdx = PApplet.parseInt(random(0, 3.99f));
    }
        
    t.rr += 1;
    t.alphaVal -= 20.0f/( log(t.follower + 10)*3 );
    
    pushStyle();
    noFill();
    
    // ring animation
    if(blink){
      if (t.rt) stroke(0xff00FF00, t.alphaVal);
      else stroke(0xffFFFFFF, t.alphaVal);
      strokeWeight(3);
      ellipse(t.x, t.y, t.rr, t.rr);
    }
    
    noStroke();
    
    // green stroke for RT - DEPRECATED
//    if (t.rt) {
//      //fill(palette[4], t.alphaVal);
//      strokeWeight(2);
//      stroke(palette[4], t.alphaVal);
//    }
    
    if (t.sentiment > 0) {
      fill(0, t.sentiment*10, 100, t.alphaVal);
    }else if(t.sentiment == 0){
      fill(0xffFFFFFF, t.alphaVal);
    }
    else {
      fill(228, -10*t.sentiment, 100, t.alphaVal);
    }
    
    // rain drop
    ellipse(t.x, t.y, t.r, t.r);
    textFont(font, 17);
    text(t.location, t.x, t.y, 120, 100);
    //text(t.follower, t.x, t.y);
    //text(t.msg, t.x, t.y, 120, 100);
    //text(t.sentiment, t.x, t.y, 120, 100);
    
    //t.alphaVal--;
    popStyle();
  }
  

  
//  // render gauge
//  pushStyle();
//  fill(#FFFFFF, 70);
//  arc(width - 100, height*0.9, 50, 50, PI, 2*PI);
//  fill(#FF0000, 95);
//  arc(width - 100, height*0.9, 48, 48, PI, PI*(1 + float(numTw)/10.0));
//  
//  textAlign(CENTER, TOP);
//  fill(#999999, 90);
//  text(numTw + "tweets/s", width-100, height*0.9 + 10);
//  popStyle();
}
public void drawRTUpdate() {
  // every 1 sec
  if ( millis()-t1 > 1000 ) {
    numTw = bucTw;
    bucTw = 0;
    // reset t0
    t1 = millis();
  }

  // spacing
  float tall = h - hBorder*4;
  float wStep = w/(20 * rtNumShow);
  int rGap = 60;
  int numLogPanel = 6;
  float wBorder = w*0.8f;
  int hHead = 3*hBorder;
  int hTail = h - hBorder;
  float hWidth = hTail - hHead;
  float barThickness = w*0.03f;

  // draw timeline
  pushStyle();
  fill(0xffBBBBBB);
  textAlign(LEFT, TOP);
  textSize(15);
  text("Posted Since..", w - hBorder - rGap - 0.05f*w, 2*hBorder);
  popStyle();

  // time-line vis
  pushAll();
  textSize(12);
  for (int i = 0; i < numLogPanel; i++ ) {
    for (int j = 1; j < 10; j++ ) {
      float pX = w - hBorder - rGap; // 20 px for label
      float pY = 3*hBorder + i*tall/numLogPanel + log10(j)*(tall/numLogPanel);

      if (j == 1) {
        stroke(0xffFFFFFF);
        //text(pow( 10, i)+" Min", w-hBorder-rGap, pY);
        text(timeLabels[i], w-hBorder-rGap, pY);
      }
      else {
        stroke(0xffAAAAAA, 95);
      }

      line(pX, pY, pX - 0.05f*w, pY);
    }
  }
  popAll();



  pushStyle();
//  int yPos = h - 250;
//  textAlign(LEFT, CENTER);
//  textSize(14);
//  //text("TOP " + rtNum + " RETWEETS", 10, yPos - 20);
//  text("TOP "+rtNumShow+" RETWEETS", 10, yPos - 20);
  
  ArrayList tempArray = (ArrayList<RT>)rtArray1.clone();
  
  // vars
  int numShow = min(tempArray.size(), rtNumShow);
  int total = 0;
  float[] percentile = new float[numShow];
  
  //// TO DEBUG
  //println(numShow + "<<" + tempArray.size() + "|" + rtNumShow);
  for ( int i = 0; i < numShow; i++ ) {
    RT rtweet;
    int numrt;
    int numrtMax;
    boolean nullException = false;
    try {
      
      rtweet = (RT)tempArray.get(i);
      //// TO DEBUG
      //println("=====");
      //println(rtweet.toString());
      ////
      numrt = (int)rtweet.getRtCount();  // DEBUG HERE!!!
      numrtMax = constrain(numrt/100, 1, PApplet.parseInt(0.8f*w));
    } catch (Exception e){
      print(e);
      print("    ---> tempArray size: ");
      println(tempArray.size());
      
      // remove null exceptioned element from the array
      nullException = true;
      tempArray.remove(i);
      i--;
      continue;
    }
    
    // timeline grid
    float xStep = (w-(2 * hBorder))/2.0f * i/24.0f;
    //int rx = int(hBorder + min(rtweet.ageLog, 24) * xStep);
    float rx = 0.2f*w + i*0.05f*w;
    int ry = PApplet.parseInt(3*hBorder + rtweet.ageLog10 * tall/numLogPanel);

    // timeline plot (DOTS)
    noStroke();
    fill(rank10[i]);
    rectMode(CORNER);
    rect( w - hBorder - rGap - (rtNumShow-i)*wStep, ry, wStep, wStep ); // dots on the time line
    
    // compute real-time ratio
    percentile[i] = (float)rtweet.newTw;
    total += rtweet.newTw;
    
    // update Age
    rtweet.updateAge();

  }
  
  // proceed if no exception
  //if (!nullException){
  
  totalItems = total;
  
  // sliding animation
  buf2[10] = hTail;
  int prevX = hHead;
  int nextX = hHead;
  
  textAlign(LEFT, TOP);
  textFont(font, 18); //16
  
  
  for ( int i = 0; i < numShow; i++ ) {
    RT rtweet;
    try {
      rtweet = (RT)tempArray.get(i);
    } catch (Exception e){
      print(e);
      print("\t");
      println(tempArray.size());
      continue;
    }

    
    percentile[i] = hWidth * (percentile[i] / (float)total);
    nextX += (int) percentile[i];

    fill(rank10[i]);
    
    // split animation (Converging algorithm)
    float xStart = hHead, xEnd = hTail;
    if ( i > 0 ) xStart = buf1[i] + PApplet.parseInt((prevX - buf1[i])*0.05f);
    if ( i < (numShow-1) ){
      xEnd = buf2[i] + PApplet.parseInt((nextX - buf2[i])*0.05f);
      buf2[i] = xEnd;
    }
    
    // boxes
    buf1[i] = xStart;
    buf2[i] = xEnd; //NEW
    
    noStroke();
    rect(hBorder, xStart, barThickness, xEnd - xStart);
    stroke(0xffAAAAAA);
    line(hBorder, xStart, hBorder + barThickness, xStart);
    text((String)rtweet.getText(), hBorder + barThickness + 70, xStart, w*0.7f, (xEnd-xStart));
    
    // labels
    fill(0xff000000);
    float lwidth = textWidth( Integer.toString(rtweet.newTw) ); // label width
    boolean showIt = 16 < (xEnd - xStart);  // if the label fits in the split
    if (showIt) {
      // total rt count
      fill(palette[1]);
      text((int)rtweet.getRtCount(), hBorder + barThickness + 10, xStart);
      // new rt count
      fill(0xff000000);
      if (rtweet.newTw > burstThreshold ) fill(0xffFF0000);
      text(Integer.toString(rtweet.newTw), hBorder+5, xStart);
    }
    
    
    prevX = nextX;
  }
  //} // END proceed if no exception
  
  popStyle();
}

public void drawRT() {
  // every 1 sec
  if ( millis()-t1 > 1000 ) {
    numTw = bucTw;
    bucTw = 0;
    // reset t0
    t1 = millis();
  }

  // spacing
  float tall = h - hBorder*4;
  float wStep = w/(20 * rtNumShow);
  int rGap = 60;
  int numLogPanel = 6;
  float wBorder = w*0.8f;
  int hHead = 3*hBorder;
  int hTail = h - hBorder;
  float hWidth = hTail - hHead;
  float barThickness = w*0.08f;


  // draw timeline
  pushStyle();
  fill(0xffBBBBBB);
  textAlign(LEFT, TOP);
  textSize(15);
  text("Posted Since..", w - hBorder - rGap - 0.05f*w, 2*hBorder);
  popStyle();

  pushAll();
  textSize(12);
  for (int i = 0; i < numLogPanel; i++ ) {
    for (int j = 1; j < 10; j++ ) {
      float pX = w - hBorder - rGap; // 20 px for label
      float pY = 3*hBorder + i*tall/numLogPanel + log10(j)*(tall/numLogPanel);

      if (j == 1) {
        stroke(0xffFFFFFF);
        //text(pow( 10, i)+" Min", w-hBorder-rGap, pY);
        text(timeLabels[i], w-hBorder-rGap, pY);
      }
      else { 
        stroke(0xffAAAAAA, 95);
      }

      line(pX, pY, pX - 0.05f*w, pY);
    }
  }
  popAll();
  
  



  pushStyle();
//  int yPos = h - 250;
//  textAlign(LEFT, CENTER);
//  textSize(14);
//  //text("TOP " + rtNum + " RETWEETS", 10, yPos - 20);
//  text("TOP "+rtNumShow+" RETWEETS", 10, yPos - 20);
  
  ArrayList tempArray = (ArrayList<RT>)rtArray2.clone();
  
  // vars
  int numShow = min(tempArray.size(), rtNumShow);
  float total = 0;
  float[] percentile = new float[numShow];
  
  //for ( int i = 0; i < rtArray.size(); i++ ) {
  for ( int i = 0; i < numShow; i++ ) {
    RT rtweet;
    int numrt;
    int numrtMax;
    boolean nullException = false;
    try {
      rtweet = (RT)tempArray.get(i);
      numrt = (int)rtweet.getRtCount(); //// NULL POINTER EXCEPTION
      numrtMax = constrain(numrt/100, 1, PApplet.parseInt(0.8f*w));
    } catch (Exception e){
      print(e);
      print("    ---> tempArray size: ");
      println(tempArray.size());
      
      // remove null exceptioned element from the array
      nullException = true;
      tempArray.remove(i);
      i--;
      continue;
    }
    float xStep = (w-(2 * hBorder))/2.0f * i/24.0f;
    //int rx = int(hBorder + min(rtweet.ageLog, 24) * xStep);
    float rx = 0.2f*w + i*0.05f*w;
    int ry = PApplet.parseInt(3*hBorder + rtweet.ageLog10 * tall/numLogPanel);

    // timeline plot (DOTS)
    noStroke();
    fill(rankRT[i]);
    rectMode(CORNER);
    rect( w - hBorder - rGap - (rtNumShow-i)*wStep, ry, wStep, wStep ); // dots on the time line
    
    // compute real-time ratio
    percentile[i] = (float)rtweet.rtCount;
    total += rtweet.rtCount;
    
    // update Age
    rtweet.updateAge();

  }
  
  totalItems = (int)total;
  
  // bar animation
  buf2[10] = hTail;
  int prevX = hHead;
  int nextX = hHead;
  
  textAlign(LEFT, TOP);
  textFont(font, 18); // 16
  
  
  for ( int i = 0; i < numShow; i++ ) {
    RT rtweet;
    try {
      rtweet = (RT)tempArray.get(i);
    } catch (Exception e){
      print(e);
      print("\t");
      println(tempArray.size());
      continue;
    }
    
    percentile[i] = hWidth * (percentile[i] / total); // original
    // percentile[i] = hWidth * (log10(percentile[i])+1 / log10(total)+numShow); // log-scale
    nextX += (int) percentile[i];

    fill(rankRT[i]);
    
    // split animation (Converging algorithm)
    float xStart = hHead, xEnd = hTail;
    if ( i > 0 ) xStart = buf1[i] + PApplet.parseInt((prevX - buf1[i])*0.05f);
    if ( i < (numShow-1) ){
      xEnd = buf2[i] + PApplet.parseInt((nextX - buf2[i])*0.05f);
      buf2[i] = xEnd;
    }
    
    // boxes
    buf1[i] = xStart;
    
    noStroke();
    rect(hBorder, xStart, barThickness, xEnd - xStart);
    stroke(0xffAAAAAA);
    line(hBorder, xStart, hBorder + barThickness, xStart);
    text((String)rtweet.getText(), hBorder + barThickness + 10, xStart, w*0.75f, (xEnd-xStart));
    fill(palette[1]);
    //text((int)rtweet.getRtCount(), hBorder + barThickness + 10, xStart);
    
    // labels
    fill(0xff000000);
    float lwidth = textWidth( Integer.toString(rtweet.newTw) ); // label width
    boolean showIt = 16 < (xEnd - xStart);  // if the label fits in the split
    if (showIt) text(Long.toString(rtweet.rtCount), hBorder+5, xStart);
    
    
    prevX = nextX;
  }
  popStyle();
}

public void drawHT() {
  // every 1 sec
  if ( millis()-t1 > 1000 ) {
    numTw = bucTw;
    bucTw = 0;
    // reset t0
    t1 = millis();
  }

  // spacing
  float tall = h - hBorder*4;
  float wStep = w/(20 * rtNumShow);
  int rGap = 60;
  int numLogPanel = 6;
  float wBorder = w*0.8f;
  int hHead = 3*hBorder;
  int hTail = h - hBorder;
  float hWidth = hTail - hHead;
  float barThickness = w*0.08f;


  // draw timeline
  pushStyle();
  fill(0xffBBBBBB);
  textAlign(LEFT, TOP);
  textSize(15);
  text("Posted Since..", w - hBorder - rGap - 0.05f*w, 2*hBorder);
  popStyle();

  pushAll();
  textSize(12);
  for (int i = 0; i < numLogPanel; i++ ) {
    for (int j = 1; j < 10; j++ ) {
      float pX = w - hBorder - rGap; // 20 px for label
      float pY = 3*hBorder + i*tall/numLogPanel + log10(j)*(tall/numLogPanel);

      if (j == 1) {
        stroke(0xffFFFFFF);
        //text(pow( 10, i)+" Min", w-hBorder-rGap, pY);
        text(timeLabels[i], w-hBorder-rGap, pY);
      }
      else { 
        stroke(0xffAAAAAA, 95);
      }

      line(pX, pY, pX - 0.05f*w, pY);
    }
  }
  popAll();
  
  



  pushStyle();
//  int yPos = h - 250;
//  textAlign(LEFT, CENTER);
//  textSize(14);
//  //text("TOP " + rtNum + " RETWEETS", 10, yPos - 20);
//  text("TOP "+rtNumShow+" RETWEETS", 10, yPos - 20);
  
  ArrayList tempArray = (ArrayList<RT>)htArray.clone();
  
  // vars
  int numShow = min(tempArray.size(), htNumShow);
  float total = 0;
  float[] percentile = new float[numShow];
  
  //for ( int i = 0; i < rtArray.size(); i++ ) {
  for ( int i = 0; i < numShow; i++ ) {
    HT htag;
    int numrt;
    int numrtMax;
    boolean nullException = false;
    try {
      htag = (HT)tempArray.get(i);
      numrt = (int)htag.getHtCount(); //////////////////////// ERROR! apply exception!
      numrtMax = constrain(numrt/100, 1, PApplet.parseInt(0.8f*w)); ////////////////////////
    } catch (Exception e){
      print(e);
      print("    ---> tempArray size: ");
      println(tempArray.size());
      
      // remove null exceptioned element from the array
      nullException = true;
      tempArray.remove(i);
      i--;
      continue;
    }

    float xStep = (w-(2 * hBorder))/2.0f * i/24.0f;
    float rx = 0.2f*w + i*0.05f*w; ////////////////////////
    int ry = PApplet.parseInt(3*hBorder + htag.ageLog10 * tall/numLogPanel); ////////////////////////

    // timeline plot (DOTS)
    noStroke();
    fill(rankHT[i]);
    rectMode(CORNER);
    rect( w - hBorder - rGap - (rtNumShow-i)*wStep, ry, wStep, wStep ); // dots on the time line
    
    // compute real-time ratio
    percentile[i] = (float)htag.htCount;
    total += htag.htCount;
    
    // update Age
    htag.updateAge();
  }
  popStyle();
  
  totalItems = (int)total;
  
  // bar animation
  buf2[10] = hTail;
  int prevX = hHead;
  int nextX = hHead;
  
  textAlign(LEFT, TOP);
  textFont(font, 16);
  
  
  for ( int i = 0; i < numShow; i++ ) {
    HT htag;
    try {
      htag = (HT)tempArray.get(i);
    } catch (Exception e){
      print(e);
      print("\t");
      println(tempArray.size());
      continue;
    }
    
    int txtSize = constrain((int)((percentile[i]/total)*400), 16, 90);
    
    percentile[i] = hWidth * (percentile[i] / total);
    nextX += (int) percentile[i];

    fill(rankHT[i]);
    
    // split animation (Converging algorithm)
    float xStart = hHead, xEnd = hTail;
    if ( i > 0 ) xStart = buf1[i] + PApplet.parseInt((prevX - buf1[i])*0.05f);
    if ( i < (numShow-1) ){
      xEnd = buf2[i] + PApplet.parseInt((nextX - buf2[i])*0.05f);
      buf2[i] = xEnd;
    }
    
    // boxes
    buf1[i] = xStart;
    
    pushStyle();
    noStroke();
    rect(hBorder, xStart, barThickness, xEnd - xStart);
    stroke(0xffAAAAAA);
    line(hBorder, xStart, hBorder + barThickness, xStart);
    
    textFont(font1, txtSize);
    text((String)htag.getText(), hBorder + barThickness + 10, xStart, w*0.7f, (xEnd-xStart));
    popStyle();
    
    // labels
    pushStyle();
    fill(0xff000000);
    float lwidth = textWidth( Integer.toString(htag.getHtCount()) ); // label width
    boolean showIt = 16 < (xEnd - xStart);  // if the label fits in the split
    if (showIt) {
      if (htag.htCount > htBurstThreshold ) fill(0xffFF0000);
      text(Long.toString(htag.htCount), hBorder+5, xStart);
    }
    popStyle();
    
    
    prevX = nextX;
  }
}

public void drawMap() {
  background(0xffC8C8C8);
  map.draw();
  
  // display geolocation on mouse tip
  //de.fhpotsdam.unfolding.geo.Location location = map.getLocation(mouseX, mouseY);
  //fill(0);
  //text(location.getLat() + ", " + location.getLon(), mouseX, mouseY);
  
  //de.fhpotsdam.unfolding.geo.Location center = new de.fhpotsdam.unfolding.geo.Location(0.0f, 0.0f);
  //map.zoomAndPanTo(center, 2);
  //float maxPanningDistance = 100; // in km
  //map.setPanningRestriction(center, maxPanningDistance);
  
  //map.zoomAndPanTo(new Location(52.5f, 13.4f), 10); // zoom in
  
  /** draw markers **/
  for (int i = 0; i < twArray.size(); i++) {
    Tweet t = (Tweet) twArray.get(i);
    de.fhpotsdam.unfolding.geo.Location l = new de.fhpotsdam.unfolding.geo.Location(t.lat, t.lon);
    //de.fhpotsdam.unfolding.marker.SimplePointMarker marker = new de.fhpotsdam.unfolding.marker.SimplePointMarker(l);
    mk = new de.fhpotsdam.unfolding.marker.SimplePointMarker(l);
    
    ScreenPosition mkPos = mk.getScreenPosition(map);
    
    // draw markers
    if (t.sentiment > 0) {
      //fill(0, t.sentiment*10, 100, 100);
      fill(0xffFF0000, 100);
    }else if(t.sentiment == 0){
      fill(0xff000000, 100);
    }
    else {
      //fill(228, -10*t.sentiment, 100, 100);
      fill(0xff0000FF, 100);
    }
    
    // rain drop
    ellipse(mkPos.x, mkPos.y, t.r/2, t.r/2);
    fill(0xff000000, 255.0f);
    ellipse(mkPos.x, mkPos.y, 2, 2);
    
    //////
//    strokeWeight(5);
//    stroke(#FF0000, 100);
//    
//    noFill();
//    ellipse(mkPos.x, mkPos.y, t.r, t.r);
    /////
//    
//    map.addMarkers(mk);
//    
//    // marker style
//    marker.setColor(color(#FF0000));
//    marker.setStrokeColor(color(255, 0, 0));
//    marker.setStrokeWeight(1);
      
      
//    if (t.alphaVal < 3.0) {
//      twArray.remove(i);
//      i--;
//    }
  }

}
public void elapsedTimeVis(){
  pushAll();
  fill(0xffFFFFFF, 90);
  textFont(font1, 80);
  textAlign(RIGHT, TOP);
  text(getElapsed(millis()-t0) +"\n" + totalItems, w*0.85f, h*0.15f);//250, -200);
  popAll();
  fill(0xff444444);
}

public long elapsedMin(){
  return (millis() - t0)/(1000*60);
}

public String getCurrentTime(){
  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  //get current date time with Date()
  Date date = new Date();
  return dateFormat.format(date);
}

public static String getElapsed(long mils) {
    SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");

    String strDate = sdf.format(mils);
    return strDate;
}

public float log10 (float x) {
  return (log(x) / log(10));
}

public int getSentiment( String t ){
  StringTokenizer st = new StringTokenizer(t);
  int pos = 0;
  int neg = 0;
  
  while(st.hasMoreTokens())
  {
    String temp=st.nextToken();
    char c = temp.charAt(0);
    
    if (temp == "RT" || c == '@') continue;
    // positivity
    for ( int i=0; i<posCorpus.length; i++ ) {
      try{
        if(temp.equalsIgnoreCase(posCorpus[i])) pos++;
      }catch (Exception e){
        println("error 1");
      }
    }
    
    // negativity
    for ( int i=0; i<negCorpus.length; i++ ) {
      try{
        if(temp.equalsIgnoreCase(negCorpus[i])) neg++;
      }catch (Exception e){
        println("\terror 2");
      }        
    }
  }

  return pos - neg;
}

public boolean htExist ( String t, long mins, String time ) {
  for ( int i = 0; i < htArray.size(); i++ ) {
    HT htag = (HT)htArray.get(i);
    if ( htag.equals(t) ) {
      // if exists, increase count by 1
      htag.update(mins, time);
      return true;
    }
  }
  return false;
}

public boolean rtExist ( ArrayList rtArray, long tid, long rtCount ) {
  for ( int i = 0; i < rtArray.size(); i++ ) {
    RT rtweet = (RT)rtArray.get(i);
    if ( rtweet.equals(tid) ) {
      // if exists, update rtCount
      rtweet.updateRtCount(rtCount);
      return true;
    }
  }
  return false;
}

public void trimTweets() {
  for ( int i = 0; i < twArray.size(); i++ ) {
    Tweet tw = (Tweet)twArray.get(i);
    if ( tw.elapsed() ) {
      // if elapsed more than 30 secs since received, remove it
      twArray.remove(i);
      i--;
    }
  }
}

// Modified from
// http://jeromejaglale.com/doc/java/twitter
// J\u00e9r\u00f4me Jaglale
public static long getAge(String dateStr) {
  // parse Twitter date
  SimpleDateFormat dateFormat = new SimpleDateFormat(
  "EEE MMM dd HH:mm:ss ZZZ yyyy", Locale.ENGLISH);
  //e.g. Tue Apr 30 22:43:53 PDT 2013
  dateFormat.setLenient(false);
  Date created = null;
  try {
    created = dateFormat.parse(dateStr);
  } 
  catch (Exception e) {
    return -1;
  }

  // today
  Date today = new Date();

  // how much time since (ms)
//  long duration = today.getTime() - created.getTime(); // millisec
  long duration = (today.getTime() - created.getTime())/(1000*60); // in minute
  return duration;
}

public void pushAll(){
  pushMatrix();
  pushStyle();
}

public void popAll(){
  popMatrix();
  popStyle();
}

public void controlEvent(ControlEvent theEvent) {
  if(theEvent.isAssignableFrom(Textfield.class)) {
    println("controlEvent: accessing a string from controller '"
            +theEvent.getName()+"': "
            +theEvent.getStringValue()
            );
    String tmpString = "" + theEvent.getStringValue();
    keywords = tmpString.split(",");
    
    // reset the title
    for (int i = 0; i < keywords.length; i++ ) {
      title = keywords[i] + " ";
    }
    
    // refresh stream
    twitter.filter(new FilterQuery().track(keywords));
    
    // clear memory
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    
    // reset timer
    t0 = millis();
    
    // reset view mode
    vizMode = 0;
  }
  
  if(theEvent.controller().name()=="show") {
    autoShift = !autoShift;
  }
  if(theEvent.controller().name()=="wave") {
    blink = !blink;
  }
  if(theEvent.controller().name()=="clock") {
    timewatch = !timewatch;
  }
  if(theEvent.controller().name()=="interval") {
    t2 = millis();
    s_interval = PApplet.parseInt(theEvent.controller().value());
  }
}

/** Int Ascending */
/** Int Descending */
static class NoDescCompareNew implements Comparator<RT> {

  // (DESC)
  public int compare(RT arg0, RT arg1) {
    return arg0.getNewCount() > arg1.getNewCount() ? -1 : arg0.getNewCount() < arg1.getNewCount() ? 1:0;
  }
}

static class NoAscCompare implements Comparator<RT> {

  // (ASC)
  public int compare(RT arg0, RT arg1) {
    return arg0.getRtCount() < arg1.getRtCount() ? -1 : arg0.getRtCount() > arg1.getRtCount() ? 1:0;
  }
}

/** Int Descending */
static class NoDescCompare implements Comparator<RT> {

  // (DESC)
  public int compare(RT arg0, RT arg1) {
    return arg0.getRtCount() > arg1.getRtCount() ? -1 : arg0.getRtCount() < arg1.getRtCount() ? 1:0;
  }
}

/** Int Ascending */
static class NoAscCompareHT implements Comparator<HT> {

  // (ASC)
  public int compare(HT arg0, HT arg1) {
    return arg0.getHtCount() < arg1.getHtCount() ? -1 : arg0.getHtCount() > arg1.getHtCount() ? 1:0;
  }
}

/** Int Descending */
static class NoDescCompareHT implements Comparator<HT> {

  // (DESC)
  public int compare(HT arg0, HT arg1) {
    return arg0.getHtCount() > arg1.getHtCount() ? -1 : arg0.getHtCount() < arg1.getHtCount() ? 1:0;
  }
}

/** Maintain 10 popular realtime retweets in rtArray */
public synchronized void sortNewRT(ArrayList rtArray) {

  // sort in desc
  Collections.sort(rtArray, new NoDescCompareNew());

  // temp array
  ArrayList tempArray = new ArrayList();

  int until = rtArray.size() > rtNum ? rtNum : rtArray.size();

  for ( int i = 0; i < until; i++ ) {
    RT rtweet = (RT) rtArray.get(i);
    tempArray.add(rtweet);
    // System.out.println(rtweet.rtCount);
  }

  rtArray.clear();
  rtArray.addAll(tempArray);
  tempArray.clear();
}



/** Maintain 10 popular retweets in rtArray */
public void sortRT(ArrayList rtArray) {

  // sort in desc
  Collections.sort(rtArray, new NoDescCompare());

  // temp array
  ArrayList tempArray = new ArrayList();

  int until = rtArray.size() > rtNum ? rtNum : rtArray.size();

  for ( int i = 0; i < until; i++ ) {
    RT rtweet = (RT) rtArray.get(i);
    tempArray.add(rtweet);
    // System.out.println(rtweet.rtCount);
  }

  rtArray.clear();
  rtArray.addAll(tempArray);
  tempArray.clear();
}

public void sortHashtag() {

  // sort in desc
  Collections.sort(htArray, new NoDescCompareHT());

  // temp array
  ArrayList tempArray = new ArrayList();

  int until = htArray.size() > htNum ? htNum : htArray.size();

  for ( int i = 0; i < until; i++ ) {
    HT hashtag = (HT) htArray.get(i);
    tempArray.add(hashtag);
    // System.out.println(hashtag.htCount);
  }

  htArray.clear();
  htArray.addAll(tempArray);
  tempArray.clear();
}

///** String Ascending */
//static class NameAscCompare implements Comparator<RT> {
//
//  // (ASC)
//  @Override
//    public int compare(RT arg0, RT arg1) {
//    return arg0.getText().compareTo(arg1.getText());
//  }
//}
//
///** String Descending */
//static class NameDescCompare implements Comparator<RT> {
//
//  // (DESC)
//  @Override
//    public int compare(RT arg0, RT arg1) {
//    return arg1.getText().compareTo(arg0.getText());
//  }
//}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--hide-stop", "TweetStreamProbe" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
