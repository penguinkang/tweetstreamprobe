class HT 
{
  String txt;
  String strTime;
  int htCount;
  long ageMin;
  float ageLog10;

  HT( String _txt, long _mins, String _time) {
    txt = _txt;
    this.htCount = 1;
    this.strTime = _time;
    this.ageMin = _mins;
    this.ageLog10 = log10(max(abs(this.ageMin), 1)); // log age in minutes
  }

  public int getHtCount() {
    return this.htCount;
  }

  public String getText() {
    return this.txt;
  }

  public void update(long _mins, String _time) {
    this.htCount++;
    if (_mins > this.ageMin){
      this.ageMin = _mins;
      this.ageLog10 = log10(max(abs(this.ageMin), 1));
      this.strTime = _time;
    }
  }

  public void updateAge(){
    this.ageMin = getAge(this.strTime);
    ageLog10 = log10(max(abs(this.ageMin), 1)); // log age in minutes
  }

  public boolean equals (String x) {
    if (this.txt.equals(x)) { 
      return true;
    }
    else {
      return false;
    }
  }
}

