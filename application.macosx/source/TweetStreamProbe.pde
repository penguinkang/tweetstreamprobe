 /*
 TWEETPROBE
 Twitter Stream Vizualization
 BYUNGKYU (JAY) KANG
 ver1.0 - June. 01. 2013.
 Presented and exhibited in IEEE VIS 2013 Arts Program.
 
 ver 1.1 - May. 21. 2015.
 Presented in MAT End-of-Year-Show (EOYS), University of California, Santa Barbara.
 */

//import java.text.*;
//import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Comparator;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.Map;
import processing.opengl.*;
import javax.media.opengl.*;
import peasy.*;
import controlP5.*;    // interface
import de.bezier.data.sql.*; // mysql
import de.fhpotsdam.unfolding.*; // interactive map
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.utils.*;
import de.fhpotsdam.unfolding.providers.*;

// create objects
ControlP5 controlp5;  // controlP5 object
Prop props;           // property object
MySQL db;             // mysql object
UnfoldingMap map;     // map interface object initialized
de.fhpotsdam.unfolding.marker.SimplePointMarker mk; // marker obj
 
// DEFAULT KEYWORDS TO SEARCH
String keywords[] = { "oil" };

// OAuth key containers
static String cKey;
static String cSec;
static String aToken;
static String aTokenSec;

String lang[] = {"en"};
color[] palette = { #FFFFFF, #AFEFF1, #31C4F5, #0084B4, #97CD39 }; //twitter base
color[] paletteT = { #302D40, #475956, #90A67B, #FF0000 }; // twitter
color[] paletteRT = { #FF0000, #FF7400, #CD0074, #A60000 }; // RT

int w, h; // screen size
int hBorder = 30;
PImage   img;

int      vizMode = 4; // visualization type at start
boolean  imageLoaded;
boolean  displayImg = false;
boolean  blink = true;
boolean  timewatch = true;
boolean  autoShift = false;
boolean  ui = true;

PFont font, font1, fontb, font0;
String title;
String who, time, txt, where, now, startTime;

ArrayList twArray = new ArrayList(); // Tweet Bucket
ArrayList rtArray1 = new ArrayList(); // Emerging RT Ranking view
ArrayList rtArray2 = new ArrayList(); // RT Count Ranking view
ArrayList htArray = new ArrayList(); // Hashtag Bucket

int t0, t1, t2, t3, tDelta; // timestamp
int s_interval = 30; // auto shift default interval
int velo = 10;
int maxDist = 1000;
int numTw, bucTw; // num of tweet per sec / bucket
int htNum = 30; // top N hashtags
int rtNum = 30;  // top N retweets
int rtNumShow = 10;
int htNumShow = 10;
int totalItems = 0;
int burstThreshold = 10; // BURST THRESHOLD
int htBurstThreshold = 20; // BURST THRESHOLD
float[] buf1     = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, w };
float[] buf2     = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, w };

// corpuses
String[] posCorpus, negCorpus;


void setup() {
  
  // create i/f objects
  controlp5 = new ControlP5(this);
  
  //map = new UnfoldingMap(this, new StamenMapProvider.Toner());
  //map = new UnfoldingMap(this, new EsriProvider.WorldShadedRelief());
  //map = new UnfoldingMap(this, new StamenMapProvider.TonerBackground());
  //map = new UnfoldingMap(this, new OpenWeatherProvider.Rain());
  //map = new UnfoldingMap(this, new OpenWeatherProvider.Temperature());
  map = new UnfoldingMap(this, new OpenMapSurferProvider.Grayscale());
  //map = new UnfoldingMap(this, new Microsoft.AerialProvider());
  //map = new UnfoldingMap(this, new ImmoScout.ImmoScoutProvider());
  
  //map = new UnfoldingMap(this);
  MapUtils.createDefaultEventDispatcher(this, map);
  
  // property setup
  try {
    props = new Prop();
    // load a configuration from a file inside the data folder
    props.load("conf.properties");
    
    /** Please uncomment this if you want to read width/height from property file **/
    //w = int(props.getProperty("prop.width",""+1024));
    //h = int(props.getProperty("prop.height",""+768));

    w = displayWidth;
    h = displayHeight;
    //w = 1024;
    //h = 768;
    
    // Your Oauth info
    cKey     = props.getProperty("prop.consumerKey","");
    cSec     = props.getProperty("prop.consumerSecret","");
    // Your Access Token info
    aToken    = props.getProperty("prop.accessToken","");
    aTokenSec = props.getProperty("prop.accessTokenSecret","");
    
    // debug
    println(cKey+"\n"+cSec+"\n"+aToken+"\n"+aTokenSec);
    
  }catch (Exception e) {
    println("can't find config file...");
  }
  
  // database setup
  String user     = "root";
  String pass     = "kang4848";
  String database = "geocode";

  db = new MySQL( this, "localhost", database, user, pass );  // open database file
  db.setDebug(false);
  
 
  // make a title
  title = "";
  for (int i = 0; i < keywords.length; i++ ) {
    title += keywords[i] + " ";
  }  
  
  // canvas setup
  size(w, h, OPENGL);
  /** deprecated **/
  //hint(DISABLE_OPENGL_2X_SMOOTH);
  //hint(ENABLE_OPENGL_4X_SMOOTH);

  noStroke();
  imageMode(CORNER);
  colorMode(HSB, 360, 100, 100);
  img = loadImage("logo.png");
  smooth();
  
  // font setup
  font = loadFont("Helvetica-Bold-22.vlw");
  fontb = loadFont("Helvetica-Bold-36.vlw");
  font0 = loadFont("Helvetica-14.vlw");
  font1 = loadFont("HelveticaNeue-Bold-180.vlw");
  textFont(font0, 14);
  strokeCap(SQUARE);
  textAlign(LEFT, CENTER);
  
  // initialize Twitter Stream session
  connectTwitter();
  twitter.addListener(listener); // start another thread
  if (keywords.length==0) twitter.sample();
  else twitter.filter(new FilterQuery().track(keywords));
  
  // start counter
  t0 = millis();
  t1 = millis();
  t2 = millis();
  t3 = millis(); // to trim tweets in view0 (raindrop)
  now = getCurrentTime();
    
  posCorpus = loadStrings("positive.txt");
  negCorpus = loadStrings("negative.txt");
  
  frameRate(60);
  
  if (ui){
    controlp5.addTextfield("query")
       .setPosition(w - hBorder - 60 - 0.05*w,20)
       .setSize(100,20)
       .setFont(font)
       .setFocus(true)
       .setColor(#ffffff)
       ;
       
    controlp5.addToggle("show",autoShift,300,10,20,20);
    controlp5.addToggle("wave",blink,340,10,20,20);
    controlp5.addToggle("clock",timewatch,380,10,20,20);
    controlp5.addSlider("interval",5,600,10,420,10,100,20);
  }
}

// displaying tweets
void arriveTw (Status s)
{
  if ( vizMode == 0 ) {
    raindrop( s );
  }
  else if( vizMode == 1 ) {
    readRTUpdate( s );
  }
  else if( vizMode == 2 ) {
    readRT( s );
  }
  else if( vizMode == 3 ) {
    readHT( s );
  }
  else if( vizMode == 4 ) {
    readLocation( s );
  }
}

