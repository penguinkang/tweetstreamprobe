//// [0] REALTIME RAIN DROP VIZ ////
void raindrop( Status s ) {
  who = s.getUser().getScreenName();

  int follower = s.getUser().getFollowersCount();
  int friend = s.getUser().getFriendsCount();
  String location = s.getUser().getLocation();

  time = s.getCreatedAt().toString();
  txt = s.getText();
  
  // sentiment extraction
  int sentiment = getSentiment( txt );
  
  boolean retweet = false;
  String[] m1 = match(txt, "RT @");
  if (m1 != null) {
    retweet = true;
  }

  GeoLocation g =s.getGeoLocation();
  if (g!=null){
    where = g.toString();
  }
  
  int rtCount = 0;
  if (s.getRetweetedStatus() != null) rtCount = (int)s.getRetweetedStatus().getRetweetCount();
  //else println("no rt!");
  twArray.add(new Tweet(txt, retweet, rtCount, follower, friend, location, sentiment));
  bucTw++;
}

//// [1] RTUpdate Process ////
void readRTUpdate( Status s ) {
  bucTw++;

  // if tw origin exists...
  if (s.getRetweetedStatus() != null) {
    String rtText = s.getRetweetedStatus().getText();
    rtText = rtText.replace("\n", "").replace("\r", "");
    int rtFollower = s.getRetweetedStatus().getUser().getFollowersCount();
    int rtFriend = s.getRetweetedStatus().getUser().getFriendsCount();
    long rtId = s.getRetweetedStatus().getId();
    long rtCount = s.getRetweetedStatus().getRetweetCount();
    String time = s.getRetweetedStatus().getCreatedAt().toString();
    
    // add to rtArray
    boolean exist = false;
    if (vizMode == 1) exist = !rtExist(rtArray1, rtId, rtCount);
    else  exist = !rtExist(rtArray2, rtId, rtCount);
    if (exist) {
      long mins = getAge(time);
      
      if (vizMode == 1) rtArray1.add(new RT(rtText, rtId, rtFollower, rtFriend, rtCount, mins, time));
      else rtArray2.add(new RT(rtText, rtId, rtFollower, rtFriend, rtCount, mins, time));
    }
    
    // sort RT when it arrives
    if (vizMode == 1) sortNewRT(rtArray1);
    else sortNewRT(rtArray2);
  }
}


//// [2] RT Process ////
void readRT( Status s ) {
  bucTw++;

  // if tw origin exists...
  if (s.getRetweetedStatus() != null) {
    String rtText = s.getRetweetedStatus().getText();
    rtText = rtText.replace("\n", "").replace("\r", "");
    int rtFollower = s.getRetweetedStatus().getUser().getFollowersCount();
    int rtFriend = s.getRetweetedStatus().getUser().getFriendsCount();
    long rtId = s.getRetweetedStatus().getId();
    long rtCount = s.getRetweetedStatus().getRetweetCount();
    String time = s.getRetweetedStatus().getCreatedAt().toString();
    
    // add to rtArray
    boolean exist = false;
    if (vizMode == 1) exist = !rtExist(rtArray1, rtId, rtCount);
    else  exist = !rtExist(rtArray2, rtId, rtCount);
    if (exist) {
      long mins = getAge(time);
      
      if (vizMode == 1) rtArray1.add(new RT(rtText, rtId, rtFollower, rtFriend, rtCount, mins, time));
      else rtArray2.add(new RT(rtText, rtId, rtFollower, rtFriend, rtCount, mins, time));
    }
    
    // sort RT when it arrives
    if (vizMode == 1) sortRT(rtArray1);
    else sortRT(rtArray2);
  }
}

//// [3] HT Process ////
void readHT( Status s ) {
  // gloval vars
//  who = s.getUser().getScreenName();

  // local vars
//  int follower = s.getUser().getFollowersCount();
//  int friend = s.getUser().getFriendsCount();
//  String profLinkColor = s.getUser().getProfileLinkColor();
//  time = s.getCreatedAt().toString();
  String time;
  if (s.getRetweetedStatus() != null) time = s.getRetweetedStatus().getCreatedAt().toString();
  else time = s.getCreatedAt().toString();
  long mins = getAge(time);
  txt = s.getText();

  // RT? - by text
  boolean retweet = false;
  String[] m1 = match(txt, "RT @");
  if (m1 != null) {
    retweet = true;
  }

  // GeoLocation
//  GeoLocation g =s.getGeoLocation();
//  if (g!=null)where = g.toString();

  bucTw++;

  // HASHTAG collection
  // String[] txts = txt.split("\\W");
  txt = txt.toUpperCase();
  txt = txt.replace("...", "");
  String[] txts = txt.split("\\s+");
  for (int i = 0; i < txts.length; i++) {
    // if HT
    if ( txts[i].startsWith("#") ) {
      txts[i] = txts[i].replace(",","").replace(".","").replace(":","");
      
      // if exists
      if (!htExist(txts[i], mins, time)) {
        
        // add to the list
        htArray.add(new HT(txts[i], mins, time));
        
        // sort HT when it arrives
        sortHashtag();
      }
    }
  }

}


//// [4] GeoCode Process ////
void readLocation( Status s ) {
  //who = s.getUser().getScreenName();

  int follower = s.getUser().getFollowersCount();
  int friend = s.getUser().getFriendsCount();
  String location = s.getUser().getLocation();

  //time = s.getCreatedAt().toString();
  txt = s.getText();
  
  // sentiment extraction
  int sentiment = getSentiment( txt );
  
  // detect RT
  boolean retweet = false;
  String[] m1 = match(txt, "RT @");
  if (m1 != null) {
    retweet = true;
  }
  
  // RT count
  int rtCount = 0;
  if (s.getRetweetedStatus() != null) rtCount = (int)s.getRetweetedStatus().getRetweetCount();
  //else println("no rt!");
  
  Tweet tw = new Tweet(txt, retweet, rtCount, follower, friend, location, sentiment);
  
  // geo only
  GeoLocation g =s.getGeoLocation();
  if (g!=null){
    where = g.toString();
    //println(where);
    tw.setGeoCoordinate(g.getLatitude(), g.getLongitude());
    twArray.add(tw);
    bucTw++;
    
  }else{
    /** @TODO: add interpretation logic here! **/
    // sql open connection
    //for (String word: location.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+")){
    for (String word: location.toLowerCase().replace("\n",",").split(",")){
      word = word.trim();
      System.out.println(word);
      if ( db.connect() ){
        //String _query = "SELECT * FROM " + "all15k" + " WHERE `name` LIKE '%"+ word + "%' LIMIT 1";
        String _query = "SELECT * FROM " + "`all`" + " WHERE LOWER(`name`) LIKE '"+ word + "' LIMIT 1"; 
        db.query( _query );
        if ( db.next() ){ // if matches
          float lat = db.getFloat("lat");
          float lon = db.getFloat("lon");
          if (lat != 0){
            tw.setGeoCoordinate(lat, lon);
            twArray.add(tw);
            bucTw++;
            println("lat:"+lat+"  lon:"+lon);
            println("=====");
            break;
          }
        }
      }
      db.close();
    }
    // getGeocode
    // sql close connection
    //tw.setGeoCoordinate(0.0,0.0);
    
    System.gc(); // manually collect garbages
  }
}
