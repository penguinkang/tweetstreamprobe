/** Int Ascending */
/** Int Descending */
static class NoDescCompareNew implements Comparator<RT> {

  // (DESC)
  public int compare(RT arg0, RT arg1) {
    return arg0.getNewCount() > arg1.getNewCount() ? -1 : arg0.getNewCount() < arg1.getNewCount() ? 1:0;
  }
}

static class NoAscCompare implements Comparator<RT> {

  // (ASC)
  public int compare(RT arg0, RT arg1) {
    return arg0.getRtCount() < arg1.getRtCount() ? -1 : arg0.getRtCount() > arg1.getRtCount() ? 1:0;
  }
}

/** Int Descending */
static class NoDescCompare implements Comparator<RT> {

  // (DESC)
  public int compare(RT arg0, RT arg1) {
    return arg0.getRtCount() > arg1.getRtCount() ? -1 : arg0.getRtCount() < arg1.getRtCount() ? 1:0;
  }
}

/** Int Ascending */
static class NoAscCompareHT implements Comparator<HT> {

  // (ASC)
  public int compare(HT arg0, HT arg1) {
    return arg0.getHtCount() < arg1.getHtCount() ? -1 : arg0.getHtCount() > arg1.getHtCount() ? 1:0;
  }
}

/** Int Descending */
static class NoDescCompareHT implements Comparator<HT> {

  // (DESC)
  public int compare(HT arg0, HT arg1) {
    return arg0.getHtCount() > arg1.getHtCount() ? -1 : arg0.getHtCount() < arg1.getHtCount() ? 1:0;
  }
}

/** Maintain 10 popular realtime retweets in rtArray */
synchronized void sortNewRT(ArrayList rtArray) {

  // sort in desc
  Collections.sort(rtArray, new NoDescCompareNew());

  // temp array
  ArrayList tempArray = new ArrayList();

  int until = rtArray.size() > rtNum ? rtNum : rtArray.size();

  for ( int i = 0; i < until; i++ ) {
    RT rtweet = (RT) rtArray.get(i);
    tempArray.add(rtweet);
    // System.out.println(rtweet.rtCount);
  }

  rtArray.clear();
  rtArray.addAll(tempArray);
  tempArray.clear();
}



/** Maintain 10 popular retweets in rtArray */
void sortRT(ArrayList rtArray) {

  // sort in desc
  Collections.sort(rtArray, new NoDescCompare());

  // temp array
  ArrayList tempArray = new ArrayList();

  int until = rtArray.size() > rtNum ? rtNum : rtArray.size();

  for ( int i = 0; i < until; i++ ) {
    RT rtweet = (RT) rtArray.get(i);
    tempArray.add(rtweet);
    // System.out.println(rtweet.rtCount);
  }

  rtArray.clear();
  rtArray.addAll(tempArray);
  tempArray.clear();
}

void sortHashtag() {

  // sort in desc
  Collections.sort(htArray, new NoDescCompareHT());

  // temp array
  ArrayList tempArray = new ArrayList();

  int until = htArray.size() > htNum ? htNum : htArray.size();

  for ( int i = 0; i < until; i++ ) {
    HT hashtag = (HT) htArray.get(i);
    tempArray.add(hashtag);
    // System.out.println(hashtag.htCount);
  }

  htArray.clear();
  htArray.addAll(tempArray);
  tempArray.clear();
}

///** String Ascending */
//static class NameAscCompare implements Comparator<RT> {
//
//  // (ASC)
//  @Override
//    public int compare(RT arg0, RT arg1) {
//    return arg0.getText().compareTo(arg1.getText());
//  }
//}
//
///** String Descending */
//static class NameDescCompare implements Comparator<RT> {
//
//  // (DESC)
//  @Override
//    public int compare(RT arg0, RT arg1) {
//    return arg1.getText().compareTo(arg0.getText());
//  }
//}

