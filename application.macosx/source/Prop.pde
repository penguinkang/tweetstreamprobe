class Prop {
  String[] lines;
  
  // load property file (relative path)
  public void load (String path){
    this.lines = loadStrings(path); 
  }
  
  public String getProperty( String propName, String defVal ){
    for (int i=0; i<lines.length; i++){
      if (lines[i].length() > 2 && !lines[i].substring(0, 1).matches("#")){
        if (lines[i].contains(propName)) return lines[i].split("=")[1];
      }
    }
    return defVal;
  }
}
