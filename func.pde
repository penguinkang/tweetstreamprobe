void elapsedTimeVis(){
  pushAll();
  fill(#FFFFFF, 90);
  textFont(font1, 80);
  textAlign(RIGHT, TOP);
  text(getElapsed(millis()-t0) +"\n" + totalItems, w*0.85, h*0.15);//250, -200);
  popAll();
  fill(#444444);
}

long elapsedMin(){
  return (millis() - t0)/(1000*60);
}

String getCurrentTime(){
  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  //get current date time with Date()
  Date date = new Date();
  return dateFormat.format(date);
}

public static String getElapsed(long mils) {
    SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");

    String strDate = sdf.format(mils);
    return strDate;
}

float log10 (float x) {
  return (log(x) / log(10));
}

int getSentiment( String t ){
  StringTokenizer st = new StringTokenizer(t);
  int pos = 0;
  int neg = 0;
  
  while(st.hasMoreTokens())
  {
    String temp=st.nextToken();
    char c = temp.charAt(0);
    
    if (temp == "RT" || c == '@') continue;
    // positivity
    for ( int i=0; i<posCorpus.length; i++ ) {
      try{
        if(temp.equalsIgnoreCase(posCorpus[i])) pos++;
      }catch (Exception e){
        println("error 1");
      }
    }
    
    // negativity
    for ( int i=0; i<negCorpus.length; i++ ) {
      try{
        if(temp.equalsIgnoreCase(negCorpus[i])) neg++;
      }catch (Exception e){
        println("\terror 2");
      }        
    }
  }

  return pos - neg;
}

boolean htExist ( String t, long mins, String time ) {
  for ( int i = 0; i < htArray.size(); i++ ) {
    HT htag = (HT)htArray.get(i);
    if ( htag.equals(t) ) {
      // if exists, increase count by 1
      htag.update(mins, time);
      return true;
    }
  }
  return false;
}

boolean rtExist ( ArrayList rtArray, long tid, long rtCount ) {
  for ( int i = 0; i < rtArray.size(); i++ ) {
    RT rtweet = (RT)rtArray.get(i);
    if ( rtweet.equals(tid) ) {
      // if exists, update rtCount
      rtweet.updateRtCount(rtCount);
      return true;
    }
  }
  return false;
}

void trimTweets() {
  for ( int i = 0; i < twArray.size(); i++ ) {
    Tweet tw = (Tweet)twArray.get(i);
    if ( tw.elapsed() ) {
      // if elapsed more than 30 secs since received, remove it
      twArray.remove(i);
      i--;
    }
  }
}

// Modified from
// http://jeromejaglale.com/doc/java/twitter
// Jérôme Jaglale
public static long getAge(String dateStr) {
  // parse Twitter date
  SimpleDateFormat dateFormat = new SimpleDateFormat(
  "EEE MMM dd HH:mm:ss ZZZ yyyy", Locale.ENGLISH);
  //e.g. Tue Apr 30 22:43:53 PDT 2013
  dateFormat.setLenient(false);
  Date created = null;
  try {
    created = dateFormat.parse(dateStr);
  } 
  catch (Exception e) {
    return -1;
  }

  // today
  Date today = new Date();

  // how much time since (ms)
//  long duration = today.getTime() - created.getTime(); // millisec
  long duration = (today.getTime() - created.getTime())/(1000*60); // in minute
  return duration;
}

void pushAll(){
  pushMatrix();
  pushStyle();
}

void popAll(){
  popMatrix();
  popStyle();
}

void controlEvent(ControlEvent theEvent) {
  if(theEvent.isAssignableFrom(Textfield.class)) {
    println("controlEvent: accessing a string from controller '"
            +theEvent.getName()+"': "
            +theEvent.getStringValue()
            );
    String tmpString = "" + theEvent.getStringValue();
    keywords = tmpString.split(",");
    
    // reset the title
    for (int i = 0; i < keywords.length; i++ ) {
      title = keywords[i] + " ";
    }
    
    // refresh stream
    twitter.filter(new FilterQuery().track(keywords));
    
    // clear memory
    twArray.clear();
    rtArray1.clear();
    rtArray2.clear();
    htArray.clear();
    totalItems = 0;
    
    // reset timer
    t0 = millis();
    
    // reset view mode
    vizMode = 0;
  }
  
  if(theEvent.controller().name()=="show") {
    autoShift = !autoShift;
  }
  if(theEvent.controller().name()=="wave") {
    blink = !blink;
  }
  if(theEvent.controller().name()=="clock") {
    timewatch = !timewatch;
  }
  if(theEvent.controller().name()=="interval") {
    t2 = millis();
    s_interval = int(theEvent.controller().value());
  }
}

