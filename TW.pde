class Tweet 
{
  String msg;
  boolean rt;
  int follower;
  int friend;
  int sentiment;

  //specific to this particular visualization.
  float x, y;
  double lat, lon;
  float alphaVal;
  float time;
  String location;
  int r; // circle radius
  int rr; // ring radius

  Tweet(String _msg, boolean _retweet, int _rtCount, int _follower, int _friend, String _location, int _sentiment) {
    msg = _msg;
    
//    this.lat = null;
//    this.lon = null;
    
    // x, y coordinates
    this.x = (int)min(100 + log10(_follower)*200, w-100);
    this.y = (int)max(h - 100 - log10(_rtCount+1)*200, 0+100); // +1
    
    rt = _retweet;
    alphaVal = 255.0;
    follower = _follower;
    r = constrain(int(log10(_follower + 10)*10), 3, 100);
    rr = 1;
    friend = _friend;
    location = _location;
    sentiment = _sentiment;
    this.time = millis();
  }
  
  void setGeoCoordinate (double lat, double lon){
    this.lat = lat;
    this.lon = lon;
  }
  
  boolean elapsed (){
    boolean del = millis() - this.time > 30*1000 ? true : false;
    return del;
  }
}

